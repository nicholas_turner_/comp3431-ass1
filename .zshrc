# ROS helpers
ROS_MASTER_FILE="/tmp/ROS-$USER-session.master"
ROS_ENV_FILE="/tmp/ROS-$USER-session.env"

ENV_FILE= # filename
ENV_FILE_MTIME=0 # modification time

function ros_master {
  local URI="$1"
  if [ -z "$URI" ]; then
    if [ -f "$ROS_MASTER_FILE" ]; then
      echo "ROS_MASTER_URI=$(cat "$ROS_MASTER_FILE")"
    fi
  else
    if ! egrep '^http://' -q <<<"$URI"; then
       local URI="http://$URI"
    fi
    if ! egrep ':[0-9]+$' -q <<<"$URI"; then
       local URI="$URI:11311"
    fi
    echo "ROS_MASTER_URI=$URI"
    echo "$URI" > "$ROS_MASTER_FILE"
  fi
}

function ros_env {
  local FILE="$1"
  if [ -z "$FILE" ]; then
    if [ -f "$ROS_ENV_FILE" ]; then
      echo "source $(cat "$ROS_ENV_FILE")"
    fi
  else
    if ! egrep -q '^/' <<<"$FILE"; then
      local FILE="$(pwd)/$FILE"
    fi
    echo "source $(cat "$ROS_ENV_FILE")"
    if [ -e "$FILE" ]; then
      echo "$FILE" > "$ROS_ENV_FILE"
    else
      echo "ros_env error: $FILE does not exist" >&2
      return 1
    fi
  fi
}

function ros_preexec {
  if [ -f "$ROS_MASTER_FILE" ]; then
    export ROS_MASTER_URI="$(cat "$ROS_MASTER_FILE")"
  fi
  if [ -f "$ROS_ENV_FILE" ]; then
    local FILE="$(cat "$ROS_ENV_FILE")"
    if [ -e "$FILE" ]; then
      if [ "$FILE" = "$ENV_FILE" ]; then
        local MTIME="$(stat -c %Y "$FILE")"
        if [ "$MTIME" -gt "$ENV_FILE_MTIME" ]; then
          echo "Note: reloading $FILE"
          ENV_FILE_MTIME="$MTIME"
          source "$FILE"
        fi
      else
        echo "Note: loading $FILE"
        ENV_FILE="$FILE"
        ENV_FILE_MTIME="$(stat -c %Y "$FILE")"
        source "$FILE"
      fi
    fi
  fi
}

function preexec {
  ros_preexec;
}
function precmd {
  ros_preexec;
}

