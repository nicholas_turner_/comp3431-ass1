#include <string> 
#include <ros/ros.h> 
#include <sensor_msgs/LaserScan.h> 
#include <pcl_ros/point_cloud.h> 
#include <pcl/point_types.h>  
/*
rosmsg show sensor_msgs/LaserScan
std_msgs/Header header
  uint32 seq
  time stamp
  string frame_id
float32 angle_min
float32 angle_max
float32 angle_increment
float32 time_increment
float32 scan_time
float32 range_min
float32 range_max
float32[] ranges
float32[] intensities

*/
//namespace {   
ros::Subscriber subLaserScan_;   
ros::Publisher pubPointCloud_;   
pcl::PointCloud<pcl::PointXYZI> pc_; 
//}  
void processLaserScan(const sensor_msgs::LaserScan &laserScan) 
{   
// Pass the header information along (including frame ID)  
std::cout << "new " << laserScan.header << " anle min= " 
	<< laserScan.angle_min << " angle_max= " 
	<< laserScan.angle_max << " angle_increment= " 
	<< laserScan.angle_increment << " range_min= " 
	<< laserScan.range_min << " range_max= " 
	<< laserScan.range_max << " " 
	<< std::endl; 
/*pc_.header = laserScan.header;    
int maxPoints = ceil((laserScan.angle_max - laserScan.angle_min) / laserScan.angle_increment) + 1;  
pc_.points.resize(maxPoints);  */  
float currentAngle = laserScan.angle_min;   
int angleIndex = 0;   
int numPoints = 0;   
float mindistanceInFront=100;
float minxPt=10;
float minyPt=10;
while(currentAngle <= laserScan.angle_max) {
      float distance = laserScan.ranges[angleIndex];          
	  // See if an obstacle was detected     
	  if (distance > laserScan.range_min && distance < laserScan.range_max) {        
	  	float xPt = cosf(currentAngle) * distance;       
	  	float yPt = sinf(currentAngle) * distance; 
		if ( abs(xPt) < 0.4 && yPt > 0.2 && abs(yPt) < 2 && mindistanceInFront>distance){
			mindistanceInFront=distance;
			minxPt=xPt;
			minyPt=yPt;
		}
	/*  pc_.points[numPoints].x = xPt;       
	  pc_.points[numPoints].y = yPt;       
	  pc_.points[numPoints].z = 0.25;       
	  pc_.points[numPoints].intensity = laserScan.intensities[angleIndex];       
	  numPoints++;  */   
	  }      
	  angleIndex++;     
	  currentAngle += laserScan.angle_increment;   
}   
if ( mindistanceInFront == 100 && minxPt == 10 & minyPt==10)
	std::cout << " long " << std::endl;
else
std::cout << "mindistanceInFront = " << mindistanceInFront 
	<< " minx= "  << minxPt  
	<< " miny= "  << minyPt << std::endl;     
//pc_.points.resize(numPoints);    
//pubPointCloud_.publish(pc_); 
}  
int main(int argc, char *argv[])  {
    ros::init(argc, argv, "testls");   
	ros::NodeHandle node;      

	// Subscribers   
	/*ros::TransportHints noDelay = ros::TransportHints().tcpNoDelay(true);   
	subLaserScan_ = node.subscribe("front_sick", 10, &processLaserScan, noDelay);   */   
ros::Subscriber sub = node.subscribe ("/scan", 1, processLaserScan);
	// Publishers   
	//pubPointCloud_ = node.advertise<pcl::PointCloud<pcl::PointXYZI> >("velodyne_obstacles", 10);    
	ros::spin();                        
	// handle incoming data     
	return 0;
} 
