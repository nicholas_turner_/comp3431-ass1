#include <iostream>
#include <stdlib.h>
#include <stdio.h>
#include <sstream>
#include <ros/ros.h>
#include <geometry_msgs/Twist.h>
#include <tf/transform_listener.h>

class RobotDriver2
{
private:
	//! The node handle we'll be using
	ros::NodeHandle nh_;
	//! We will be publishing to the "/base_controller/command" topic to issue commands
	ros::Publisher cmd_vel_pub_;
	tf::TransformListener listener_;
	double turn_rate;
	double forward_rate;
	double feedback_forward;
	double result_forward;
	double feedback_turn_distance;
	double result_turn_distance;

	std::string base_frame;
	std::string odom_frame;


public:
  //! ROS node initialization
  RobotDriver2(ros::NodeHandle &nh)
  {
    nh_ = nh;
    //set up the publisher for the cmd_vel topic
    cmd_vel_pub_ = nh_.advertise<geometry_msgs::Twist>("/cmd_vel", 1);

    // Get parameters

     	base_frame = "base_link";
		odom_frame =  "odom";
		turn_rate = 0.75;
		forward_rate = 0.25;
		feedback_forward = 0.0;
		result_forward = 0.0;
  }

  //! Loop forever while sending drive commands based on keyboard input
  bool driveKeyboard()
  {
    std::cout << "Type a command and then press enter.  "
      "Use '+' to move forward, 'l' to turn left, "
      "'r' to turn right, '.' to exit.\n";

    //we will be sending commands of type "twist"
    geometry_msgs::Twist base_cmd;

    std::string line;
    //while(nh_.ok()){


      double x1 = 0.0;
      double y1 = 0.0;
      double x2 = 0.0;
      double y2 = 0.0;

			std::getline(std::cin, line);
			if (sscanf(line.c_str(), "%lf", &x1) != 1) {
				return 1;//continue;
			}
			std::getline(std::cin, line);
			if (sscanf(line.c_str(), "%lf", &y1) != 1) {
				return 1; //continue;
			}
			std::getline(std::cin, line);
			if (sscanf(line.c_str(), "%lf", &x2) != 1) {
				return 1;//continue;
			}
			std::getline(std::cin, line);
			if (sscanf(line.c_str(), "%lf", &y2) != 1) {
				return 1; //continue;
			}

			double bearing = getBearing(x1, y1, x2, y2);
			printf("%f %f %f %f", x1, y1, x2, y2, bearing);
			/*if(x1 == 1)
				driveForwardOdom(y1);
			if(x1 == 2)
				turnOdom(y1);*/
			//base_cmd.linear.x = base_cmd.linear.y = base_cmd.angular.z = 0;
			turnOdom(bearing);
      //base_cmd.angular.z = getBearing(x1, y1, x2, y2);//


      //publish the assembled command
      //cmd_vel_pub_.publish(base_cmd);
      //cmd_vel_pub_.publish(base_cmd);
    //}
    return true;
  }

private:
  //Based on code from
  bool driveForwardOdom(double distance)
    {
      // If the distance to travel is negligble, don't even try.
      if (fabs(distance) < 0.01)
        return true;

      //we will record transforms here
      tf::StampedTransform start_transform;
      tf::StampedTransform current_transform;

      try
      {
        //wait for the listener to get the first message
        listener_.waitForTransform(base_frame, odom_frame,
                                   ros::Time::now(), ros::Duration(1.0));

        //record the starting transform from the odometry to the base frame
        listener_.lookupTransform(base_frame, odom_frame,
                                  ros::Time(0), start_transform);
      }
      catch (tf::TransformException ex)
      {
        ROS_ERROR("%s",ex.what());
        return false;
      }

      //we will be sending commands of type "twist"
      geometry_msgs::Twist base_cmd;
      //the command will be to go forward at 0.25 m/s
      base_cmd.linear.y = base_cmd.angular.z = 0;
      base_cmd.linear.x = forward_rate;

      if (distance < 0)
        base_cmd.linear.x = -base_cmd.linear.x;

      ros::Rate rate(25.0);
      bool done = false;
      while (!done && nh_.ok())
      {
        //send the drive command
        cmd_vel_pub_.publish(base_cmd);
        rate.sleep();
        //get the current transform
        try
        {
          listener_.lookupTransform(base_frame, odom_frame,
                                    ros::Time(0), current_transform);
        }
        catch (tf::TransformException ex)
        {
          ROS_ERROR("%s",ex.what());
          break;
        }
        //see how far we've traveled
        tf::Transform relative_transform =
          start_transform.inverse() * current_transform;
        double dist_moved = relative_transform.getOrigin().length();

        // Update feedback and result.
        feedback_forward = dist_moved;
        result_forward = dist_moved;
        //as_.publishFeedback(feedback_);

        if(fabs(dist_moved) > fabs(distance))
        {
          done = true;
        }
      }
      base_cmd.linear.x = 0.0;
      base_cmd.angular.z = 0.0;
      cmd_vel_pub_.publish(base_cmd);

      if (done) return true;
      return false;
    }

public:
  bool turnOdom(double radians)
    {
      // If the distance to travel is negligble, don't even try.
      if (fabs(radians) < 0.01)
        return true;

      while(radians < -M_PI) radians += 2*M_PI;
      while(radians > M_PI) radians -= 2*M_PI;

      //we will record transforms here
      tf::StampedTransform start_transform;
      tf::StampedTransform current_transform;

      try
      {
        //wait for the listener to get the first message
        listener_.waitForTransform(base_frame, odom_frame,
                                   ros::Time::now(), ros::Duration(1.0));

        //record the starting transform from the odometry to the base frame
        listener_.lookupTransform(base_frame, odom_frame,
                                  ros::Time(0), start_transform);
      }
      catch (tf::TransformException ex)
      {
        ROS_ERROR("%s",ex.what());
        return false;
      }

      //we will be sending commands of type "twist"
      geometry_msgs::Twist base_cmd;
      //the command will be to turn at 0.75 rad/s
      base_cmd.linear.x = base_cmd.linear.y = 0.0;
      base_cmd.angular.z = turn_rate;
      if (radians < 0)
        base_cmd.angular.z = -turn_rate;

      //the axis we want to be rotating by
      tf::Vector3 desired_turn_axis(0,0,1);

      ros::Rate rate(25.0);
      bool done = false;
      while (!done && nh_.ok())
      {
        //send the drive command
        cmd_vel_pub_.publish(base_cmd);
        rate.sleep();
        //get the current transform
        try
        {
          listener_.lookupTransform(base_frame, odom_frame,
                                    ros::Time(0), current_transform);
        }
        catch (tf::TransformException ex)
        {
          ROS_ERROR("%s",ex.what());
          break;
        }
        tf::Transform relative_transform =
          start_transform.inverse() * current_transform;
        tf::Vector3 actual_turn_axis =
          relative_transform.getRotation().getAxis();
        double angle_turned = relative_transform.getRotation().getAngle();

        // Update feedback and result.
        feedback_turn_distance = angle_turned;
        result_turn_distance = angle_turned;


        if ( fabs(angle_turned) < 1.0e-2) continue;

        //if ( actual_turn_axis.dot( desired_turn_axis ) < 0 )
        //  angle_turned = 2 * M_PI - angle_turned;

        if (fabs(angle_turned) > fabs(radians)) done = true;
      }
      if (done) return true;
      return false;
    }

private:
	/*Returns the relative bearing from (x1, y1) to (x2, y2)*/
	double getBearing(double x1, double y1, double x2, double y2){
	    double diff_x = x2 - x1, diff_y = y2 - y1;
	    if(diff_x == 0 && diff_y > 0)
	        return M_PI/2;
	    if(diff_x == 0 && diff_y < 0)
	        return M_PI/2;
	    if(diff_y == 0 && diff_x < 0)
	        return M_PI;
	    double bearing = atan(diff_y / diff_x);
	    if (diff_x > 0 && diff_y < 0)
	        bearing -= M_PI / 2;
	    if (diff_x < 0 && diff_y < 0)
	        bearing -= M_PI;
	    if (diff_x < 0 && diff_y > 0)
	        bearing -= M_PI / 2;
	    return bearing;
	}

};

int main(int argc, char** argv)
{
  //init the ROS node
  ros::init(argc, argv, "robot_driver");
  ros::NodeHandle nh;

  RobotDriver2 driver(nh);
  driver.driveKeyboard();
}
