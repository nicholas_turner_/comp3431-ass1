#include <ros/ros.h>
#include <sensor_msgs/PointCloud2.h>
//#include <laser_geometry/laser_geometry.h>
// PCL specific includes
#include <pcl/ros/conversions.h>
#include <pcl/point_cloud.h>
#include <pcl/point_types.h>

#include <pcl/filters/voxel_grid.h>
#include <pcl/filters/passthrough.h>
#include <pcl/filters/statistical_outlier_removal.h>
//#include <pcl/visualization/pcl_visualizer.h>
#include <pcl/visualization/cloud_viewer.h>

/*#include <pcl/sample_consensus/method_types.h>
#include <pcl/sample_consensus/model_types.h>
#include <pcl/segmentation/sac_segmentation.h>
*/
#include <pcl/features/normal_3d.h>
ros::Publisher pub;

/*laser_geometry::LaserProjection projector_;

void scanCallback (const sensor_msgs::LaserScan::ConstPtr& scan_in)
{
  sensor_msgs::PointCloud cloud;
  projector_.projectLaser(*scan_in, cloud);

  // Do something with cloud.
}
*/

 void cloud_cb (const sensor_msgs::PointCloud2ConstPtr& cloud)
//void cloud_cb (const sensor_msgs::LaserScan::ConstPtr& scan_in)
{
  // ... do data processing
/*	sensor_msgs::PointCloud cloud;
  projector_.projectLaser(*scan_in, cloud);
  sensor_msgs::PointCloud2 cloud_filtered;*/
  // Publish the data
 pcl::VoxelGrid<sensor_msgs::PointCloud2> sor;
  sor.setInputCloud (cloud);
  sor.setLeafSize (0.04, 0.04, 0.04);
  sor.filter (cloud_filtered);
 // pub.publish (cloud_filtered);
  


  //pcl::PointCloud<pcl::PointXYZ>::ConstPtr cloudPCL;
	pcl::PointCloud<pcl::PointXYZRGB> cloudPCL1;
//   pcl::fromROSMsg (*cloud, cloudPCL1);
 pcl::fromROSMsg (cloud_filtered, cloudPCL1);

pcl::PointCloud<pcl::PointXYZRGB>::ConstPtr cloudPCL(new pcl::PointCloud<pcl::PointXYZRGB>(cloudPCL1));

pcl::PointCloud<pcl::PointXYZRGB>::Ptr cloudPCL2 (new pcl::PointCloud<pcl::PointXYZRGB>);
pcl::PointCloud<pcl::PointXYZRGB>::Ptr cloudPCL3 (new pcl::PointCloud<pcl::PointXYZRGB>);
pcl::PointCloud<pcl::PointXYZRGB>::Ptr cloudPCL4 (new pcl::PointCloud<pcl::PointXYZRGB>);
//PassThrough filter
pcl::PassThrough<pcl::PointXYZRGB> fil;
fil.setInputCloud(cloudPCL);
fil.setFilterFieldName("z");
fil.setFilterLimits(0.0,2.0);
fil.filter(*cloudPCL2);

pcl::PassThrough<pcl::PointXYZRGB> fil1;
fil1.setInputCloud(cloudPCL2);
fil1.setFilterFieldName("x");
fil1.setFilterLimits(-1.5,1.5);
fil1.filter(*cloudPCL3);
pcl::PassThrough<pcl::PointXYZRGB> fil2;
fil2.setInputCloud(cloudPCL2);
fil2.setFilterFieldName("y");
fil2.setFilterLimits(-0.15,0.50);
fil2.filter(*cloudPCL4);
int red = ((int)255) << 16 | ((int)0) << 8 | ((int)0);
for (size_t i=0; i<cloudPCL4->points.size(); ++i){
 if (cloudPCL4->points[i].z<1.0){
	cloudPCL4->points[i].rgba=red;}

}
std::cout << "new " << cloudPCL4->points.size()<< std::endl;

/*for (size_t i=0; i<cloudPCL4->points.size(); ++i){
 	std::cout << "    " << cloudPCL4->points[i].x
              << " "    << cloudPCL4->points[i].y
              << " "    << cloudPCL4->points[i].z << std::endl;

}
*/

//create the filtering object
/*pcl::StatisticalOutlierRemoval<pcl::PointXYZRGB> p;
p.setInputCloud (cloudPCL);
p.setMeanK (50);
p.setStddevMulThresh (1.0);
p.filter(*cloudPCL2);
*/
//SACsegmentation
 /* pcl::ModelCoefficients::Ptr coefficients (new pcl::ModelCoefficients);
  pcl::PointIndices::Ptr inliers (new pcl::PointIndices);
  // Create the segmentation object
  pcl::SACSegmentation<pcl::PointXYZRGB> seg;
  // Optional
  seg.setOptimizeCoefficients (true);
  // Mandatory
  seg.setModelType (pcl::SACMODEL_PLANE);
  seg.setMethodType (pcl::SAC_RANSAC);
  seg.setDistanceThreshold (0.01);

  seg.setInputCloud (cloudPCL->makeShared ());
  seg.segment (*inliers, *coefficients);
*/
//Normal estimation
/*
pcl::NormalEstimation<pcl::PointXYZRGB, pcl::Normal> seg;
pcl::PointCloud<pcl::Normal>::Ptr cn (new pcl::PointCloud<pcl::Normal>);
seg.setInputCloud (cloudPCL);
//seg.SetRadiusSearch (0.01);
seg.setKSearch(4);
seg.compute(*cn);
*/
/*
pcl::EuclideanClusterExtraction<pcl::PointXYZRGB> seg;
seg.setInputCloud (cloudPCL);
seg.setClusterTolerance (0.05);
seg.setMinClusterSize (1);
*/
//Visualization tools
/*
pcl::visualization::PCLVisualizer viewer("3D Viewer");
viewer.addCoordinateSystem(1.0f);
viewer.addPointCloud<pcl::PointXYZRGB>(cloudPCL4, "point cloud");
viewer.setBackgroundColor(0,0,0);
viewer.setPointCloudRenderingProperties(pcl::visualization::PCL_VISUALIZER_POINT_SIZE, 3, "point cloud");
viewer.initCameraParameters(); 
viewer.spin();*/
/*pcl::visualization::CloudViewer viewer("Simple Viewer");
//viewer.showCloud(cloudPCL);

while (!viewer.wasStopped()){
viewer.showCloud(cloudPCL);
//sleep(3);
}*/
/*
pcl::visualization::CloudViewer viewer1(" Viewer");

while (!viewer1.wasStopped()){
viewer1.showCloud(cloudPCL4);

}*/
}
/*

Features example 1
pcl::NormalEstimation<T> p;
p.setInputCloud (data);
p.SetRadiusSearch (0.01);

Features example 2
pcl::BoundaryEstimation<T,N> p;
p.setInputCloud (data);
p.setInputNormals (normals);
p.SetRadiusSearch (0.01);


Features example 3
NarfDescriptor narf_descriptor(&range_image);
narf_descriptor.getParameters().support_size = 0.3;
narf_descriptor.getParameters().rotation_invariant = false;
PointCloud<Narf36> narf_descriptors;
narf_descriptor.compute(narf_descriptors);

Segmentation example 1
pcl::SACSegmentation<T> p;
p.setInputCloud (data);
p.setModelType (pcl::SACMODEL_PLANE);
p.setMethodType (pcl::SAC_RANSAC);
p.setDistanceThreshold (0.01);

Segmentation example 2
pcl::EuclideanClusterExtraction<T> p;
p.setInputCloud (data);
p.setClusterTolerance (0.05);
p.setMinClusterSize (1);

Segmentation example 3
pcl::SegmentDifferences<T> p;
p.setInputCloud (source);
p.setTargetCloud (target);
p.setDistanceThreshold (0.001);

Visualization tools
PCLVisualizer viewer("3D Viewer");
viewer.addCoordinateSystem(1.0f);
viewer.addPointCloud(point_cloud, “our point cloud");
viewer.spin();

*/

int
main (int argc, char** argv)
{
  // Initialize ROS
  ros::init (argc, argv, "testpcl");
  ros::NodeHandle nh;

  // Create a ROS subscriber for the input point cloud
ros::Subscriber sub = nh.subscribe ("/camera/depth_registered/points_throttle", 1, cloud_cb);
//ros::Subscriber sub = nh.subscribe ("/scan", 1, cloud_cb);
//sensor_msgs/LaserScan type os /scan node
  // Create a ROS publisher for the output point cloud
  pub = nh.advertise<sensor_msgs::PointCloud2> ("output", 1);

  // Spin
  ros::spin ();
}

