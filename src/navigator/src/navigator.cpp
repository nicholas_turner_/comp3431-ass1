#include <string>
#include <algorithm>
#include <ros/ros.h>
#include <cmath>
#include <sensor_msgs/LaserScan.h> 
#include <geometry_msgs/Twist.h>
#include <geometry_msgs/PoseWithCovarianceStamped.h>
#include <tf/transform_listener.h>
#include <assign1_2013/path.h>
#include <assign1_2013/beacons.h>
#include <navigator/Beacons.h>

using namespace std;
using geometry_msgs::Point;
bool obstacleInFront;
bool goForward;


struct example_pos {
		double x, y; // from centre of screen, range = -0.5 .. 0.5
		double h, w, factor; // height, factor for scaling
		double distanceX, distanceY, angleFi;// real distance X, Y and angle between head and beacon
	};


struct beacons {
	//unsigned which; // which beacon (0 .. 5)
	double x, y;// from centre
};

//Convenience struct for robot position
struct robot_pos {
	double x, y; // absolute x, y coordinate in the world
	double alpha; //bearing from x-axis+
	double distance; //distance to closest beacon
	double acc; //accuracy
};

struct best_fit{
	double factor, angleFi;
};
// beacon pos relative to robot
struct beacon_pos {
	double distance, angleFi;
	int which;
};

struct b_pos{
	double x, y;
};

b_pos world_beacons[6]; //The real world position of each beacon which matches our beacon_pos.which


class LaserObsDetect {

	ros::Subscriber subLaserScan_;
	ros::Publisher cmd_vel_pub_;
/*The LaserScan will set this flag to true when it detects no obstacles ahead.
								The velocity publisher should not publish any movement messages in the false state;*/
public:
	LaserObsDetect(ros::NodeHandle nh)  {
		//subLaserScan_ = nh_.subscribe<sensor_msgs::LaserScan>("/scan",1,processLaserScan,this);
		//cmd_vel_pub_ = nh.advertise<geometry_msgs::Twist>("/mobile_base/commands/velocity", 1);
		subLaserScan_ = nh.subscribe("/scan", 1, &LaserObsDetect::processLaserScan, this);
		obstacleInFront=true;
	}

	~LaserObsDetect() {
	}

public:
	void processLaserScan(const sensor_msgs::LaserScan &laserScan) {
		float currentAngle = laserScan.angle_min;
		int angleIndex = 0;
		int numPoints = 0;
		float mindistanceInFront = 100; //distance the the minimal object in front of us.  Pretends its 100 until the laser finds something close
		float minxPt = 10;
		float minyPt = 10;
		double thresholdDistance = 0.4; //the closest distance in front the robot will go to an object
		double x_threshold = 0.2; //we use trig to make a "box" in front of the laser to detect obstacles that might hit the edges of our robot
		double angleRange = 0.52; //in radians
		while (currentAngle <= laserScan.angle_max) {
			float distance = laserScan.ranges[angleIndex];
			//if(currentAngle < angleRange && currentAngle > -angleRange){	

			// See if an obstacle was detected
			if (distance > laserScan.range_min && distance < laserScan.range_max) {
				float yPt = cosf(currentAngle) * distance; //This is the distance directly ahead displaced by xPt
				float xPt = sinf(currentAngle) * distance;
				if (abs(xPt) < x_threshold && abs(yPt) < thresholdDistance
						&& mindistanceInFront > distance) {
					//set that the obstacle was detected and at what angle
					mindistanceInFront = distance;
					minxPt = xPt;
					minyPt = yPt;
				}
			}
			//}
			angleIndex++;
			currentAngle += laserScan.angle_increment;

		}
		//If mindistanceInFront < 100 at this point, it means we detected something close than our minimum range


		if (mindistanceInFront == 100 && minxPt == 10 && minyPt == 10){
			//ROS_INFO("No obstacles immediately ahead");
			obstacleInFront = false;
		}
		else{
			//ROS_INFO("Detect obstacle %f at coords %f %f", mindistanceInFront, minxPt, minyPt);
			obstacleInFront = true;
		}


	}

};

class RobotDriver {
private:
	//! The node handle we'll be using
	ros::NodeHandle nh_;
	//! We will be publishing to the "/base_controller/command" topic to issue commands
	ros::Publisher cmd_vel_pub_;
	//ros::Subscriber pos_ekf_sub;
	ros::Subscriber beacon_sub;

	tf::TransformListener listener_;
	double turn_rate, forward_rate, feedback_forward, result_forward, feedback_turn_distance, result_turn_distance;
	std::string base_frame, odom_frame;
	comp3431::Path path;
	int current_path_count;

	Point prev_odom;
	// offset to initial (arbitrary) odom position
	Point offset;
	bool have_offset;
	// drift in odom over time
	Point odom_drift;

	// current driving speed
	double turn_speed, linear_speed;

	comp3431::Beacons given_beacons; //TODO: Need to order these in the expected 0-5 ordering used for colour recognition
public:
	//! ROS node initialization
	RobotDriver(ros::NodeHandle &nh) {
		nh_ = nh;
		//set up the publisher for the cmd_vel topic
		cmd_vel_pub_ = nh_.advertise<geometry_msgs::Twist>("cmd_vel", 1);
		beacon_sub = nh_.subscribe<navigator::Beacons>("/nav_beacons", 1, &RobotDriver::recv_beacons, this);
		//pos_ekf_sub = nh_.subscribe("/robot_pose_ekf/odom",1, &RobotDriver::drive, this); //switch to odom_combined
		base_frame = "base_link";
		odom_frame = "odom";
		turn_rate = 0.3;
		forward_rate = 0.5;

		feedback_forward = 0.0;
		result_forward = 0.0;
		current_path_count = 0;

		turn_speed = 0;
		linear_speed = 0;
		have_offset = false;

		loadBeaconsFromFile();
	}

	static Point add_offset(Point p, Point const offset) {
		p.x += offset.x;
		p.y += offset.y;
		p.z += offset.z;
		double r = sqrt(p.x*p.x + p.y*p.y);
		double a = atan2(p.y, p.x);
		p.x = r * cos(a + offset.z);
		p.y = r * sin(a + offset.z);
		return p;
	}
	static Point remove_offset(Point p, Point const offset) {
		double r = sqrt(p.x*p.x + p.y*p.y);
		double a = atan2(p.y, p.x);
		p.x = r * cos(a - offset.z);
		p.y = r * sin(a - offset.z);
		p.x -= offset.x;
		p.y -= offset.y;
		p.z -= offset.z;
		return p;
	}

	void recv_beacons(navigator::Beacons beacons) {
		vector <beacon_pos> dists;
		printf("recv_beacons\n");
		for (unsigned i = 0; i < 6; ++i) {
			if (beacons.beacons[i].z >= 0) {
				beacon_pos bp;
				bp.distance = beacons.beacons[i].x;
				bp.angleFi = beacons.beacons[i].y;
				bp.which = beacons.beacons[i].z;
				dists.push_back(bp);
				printf("Beacon %d (%.2f, %.2f) at %.2f m, %d deg\n",
						bp.which, world_beacons[bp.which].x, world_beacons[bp.which].y,
						bp.distance, int(bp.angleFi * 180/M_PI));
			}
		}
		Point current_pos;
		current_pos.x = prev_odom.x + odom_drift.x;
		current_pos.y = prev_odom.y + odom_drift.y;
		current_pos.z = prev_odom.z + odom_drift.z;
		//Point current_pos = add_offset(prev_odom, odom_drift);
		Point robot_pos = position_robot(current_pos, dists);
		printf("==> Position = (%.2f, %.2f, %.2f)\n", robot_pos.x, robot_pos.y, robot_pos.z);
		odom_drift.x = robot_pos.x - prev_odom.x;
		odom_drift.y = robot_pos.y - prev_odom.y;
		odom_drift.z = robot_pos.z - prev_odom.z;
		//odom_drift = remove_offset(robot_pos, prev_odom);
	}

	/*loadBeaconsFromFile - Loads the beacon positions into our array based on Matt's input file
	 * This is for convenience as we identify each beacon by an int*/
	void loadBeaconsFromFile() {
		int num_beacons = given_beacons.beacons.size();
		if (!num_beacons) {
			ROS_INFO("No beacons are loaded");
			return;
		}
		for (int i = 0; i < num_beacons; i++) {
			comp3431::Beacon beac = given_beacons.beacons[i];
			if (beac.top == "yellow" && beac.bottom == "pink") {
				world_beacons[0].x = beac.position.x;
				world_beacons[0].y = beac.position.y;
			} else if (beac.top == "pink" && beac.bottom == "yellow") {
				world_beacons[1].x = beac.position.x;
				world_beacons[1].y = beac.position.y;
			} else if (beac.top == "blue" && beac.bottom == "pink") {
				world_beacons[2].x = beac.position.x;
				world_beacons[2].y = beac.position.y;
			} else if (beac.top == "pink" && beac.bottom == "blue") {
				world_beacons[3].x = beac.position.x;
				world_beacons[3].y = beac.position.y;
			} else if (beac.top == "green" && beac.bottom == "pink") {
				world_beacons[4].x = beac.position.x;
				world_beacons[4].y = beac.position.y;
			} else if (beac.top == "pink" && beac.bottom == "green") {
				world_beacons[5].x = beac.position.x;
				world_beacons[5].y = beac.position.y;
			}
			ROS_INFO("Loaded %f %f", beac.position.x, beac.position.y);
		}
	}


	// Localisation code
	struct order_by_angle {
		bool operator()(beacon_pos const& a, beacon_pos const& b) const {
			return a.angleFi > b.angleFi;
		}
	};

	static Point position_robot(Point cur_pos, vector <beacon_pos> beacons) {
		double x, y;
		Point rp;
		if (beacons.size() == 0) {
			return cur_pos;
		}
		if (beacons.size() == 1) {
			double xh1 = world_beacons[beacons[0].which].x;
			double yh1 = world_beacons[beacons[0].which].y;
			double cp_x = cur_pos.x, cp_y = cur_pos.y;
			closest_point_to_circle(xh1, yh1, beacons[0].distance, cp_x, cp_y, &x, &y);
			rp.x = x;
			rp.y = y;
			rp.z = atan2(yh1 - y, xh1 - x) + beacons[0].angleFi;
			return cur_pos;
		}
		if (beacons.size() > 1) {
			sort(beacons.begin(), beacons.end(), order_by_angle());

			double xh1 = world_beacons[beacons[0].which].x;
			double yh1 = world_beacons[beacons[0].which].y;
			double xh2 = world_beacons[beacons.back().which].x;
			double yh2 = world_beacons[beacons.back().which].y;
			if (circle_circle_intersection(xh1, yh1, beacons[0].distance, xh2,
					yh2, beacons.back().distance, &x, &y) == 1) {
				rp.x = x;
				rp.y = y;
				rp.z = atan2(yh1 - y, xh1 - x) + beacons[0].angleFi;
				return rp;
			} else {
				printf("2 beacons intersection failed\n");
				return cur_pos;
			}
		}
	}

	//closest_point_to_circle, Takes the centre of the circle, a radius and a point.  Returns the closest point on the circle
	static void closest_point_to_circle(double cX, double cY, double R, double pX, double pY, double *aX, double *aY){
		double vX = pX - cX;
		double vY = pY - cY;
		double magV = sqrt(vX*vX + vY*vY);
		*aX = cX + vX / magV * R;
		*aY = cY + vY / magV * R;
		printf("Closest intersect of (%f, %f) to circle (%f, %f) R=%f is (%f, %f)\n", pX, pY, cX, cY, R, *aX, *aY);
	}

	static int circle_circle_intersection(double x0, double y0, double r0,
			double x1, double y1, double r1, double *x, double *y) { //, int b1, int b2
		// Soccer field size
		double max_x = 3.0;
		double max_y = 4.5;

		double a, dx, dy, d, h, rx, ry;
		double x2, y2;
		double xi, yi, xi_prime, yi_prime;

		dx = x1 - x0;
		dy = y1 - y0;

		/* Determine the straight-line distance between the centers. */
		d = hypot(dx, dy);

		/* Check for solvability. */
		if (d > (r0 + r1)) {
			/* no solution. circles do not intersect. */
			return 0;
		}
		if (d < fabs(r0 - r1)) {
			/* no solution. one circle is contained in the other */
			return 0;
		}

		/* Determine the distance from point 0 to point 2. */
		a = ((r0 * r0) - (r1 * r1) + (d * d)) / (2.0 * d);

		/* Determine the coordinates of point 2. */
		x2 = x0 + (dx * a / d);
		y2 = y0 + (dy * a / d);

		/* Determine the distance from point 2 to either of the
		 * intersection points.
		 */
		h = sqrt((r0 * r0) - (a * a));

		/* Now determine the offsets of the intersection points from
		 * point 2.
		 */
		rx = -dy * (h / d);
		ry = dx * (h / d);

		/* Determine the absolute intersection points. */
		xi = x2 + rx;
		xi_prime = x2 - rx;
		yi = y2 + ry;
		yi_prime = y2 - ry;
		if (abs(xi) > max_x || abs(yi) > max_y) {
			if (abs(xi_prime) > max_x || abs(yi_prime) > max_y) {
				return (-1); //	both points are out of the field
			} else {
				*x = xi_prime;
				*y = yi_prime;
			}
		} else {
			if (abs(xi_prime) > max_x || abs(yi_prime) > max_y) {
				*x = xi;
				*y = yi;
			} else { // need to decide from what is left and right from your
				if (xi_prime - xi != 0) {
					if ((yi + (yi_prime - yi) * (x0 - xi) / (xi_prime - xi))
							> 0) {
						*x = xi;
						*y = yi;
					} else {
						*x = xi_prime;
						*y = yi_prime;
					}
				} else {
					if (yi_prime - yi > 0) {
						if (xi < x0) {
							*x = xi;
							*y = yi;
						} else {
							*x = xi_prime;
							*y = yi_prime;
						}
					} else {
						{
							if (xi > x0) {
								*x = xi;
								*y = yi;
							} else {
								*x = xi_prime;
								*y = yi_prime;
							}
						}
					}
				}
			}
		}

		return 1;
	}


	//! Loop forever while sending drive commands based on keyboard input
public:
	void drive(){
		//ROS_INFO("Driving!");
		if(current_path_count >= path.points.size()){
			//printf("WE ARE DONE!!!\n");
			return;
		}

		tf::StampedTransform current_transform;
		//Get the latest position we know we are at
		try {
			//wait for the listener to get the first message
			listener_.waitForTransform(odom_frame, base_frame, ros::Time::now(),
					ros::Duration(1.0));

			//record the starting transform from the odometry to the base frame
			listener_.lookupTransform(odom_frame, base_frame, ros::Time(0),
					current_transform);
		} catch (tf::TransformException const& ex) {
			ROS_ERROR("%s", ex.what());
			return;
		}
		//Get current position
		Point pos;
		pos.x = current_transform.getOrigin().getX();
		pos.y = current_transform.getOrigin().getY();
		pos.z = normalize_angle(tf::getYaw(current_transform.getRotation()));

		if(!have_offset) {
			offset.x = -pos.x, offset.y = -pos.y, offset.z = -pos.z;
			printf("Offset = %f %f %f\n", offset.x, offset.y, offset.z);
			have_offset = true;
			return;
		} else {
			printf("pos0 = %.2f %.2f %.2f   drift = %.2f %.2f %.2f\n",
					pos.x, pos.y, pos.z, odom_drift.x, odom_drift.y, odom_drift.z);
			pos = add_offset(pos, offset);
			prev_odom = pos;
			//pos = add_offset(pos, odom_drift);
			pos.x += odom_drift.x, pos.y += odom_drift.y, pos.z += odom_drift.z;
		}

		//Where is the next goal point
		geometry_msgs::Point current_goal = path.points[current_path_count];
		double x_goal = current_goal.x;
		double y_goal = current_goal.y;

		//work out angle to goal
		double goal_angle = atan2(y_goal - pos.y, x_goal - pos.x);
		double angle_to_goal = goal_angle - pos.z;
		angle_to_goal = normalize_angle(angle_to_goal);

		double distance_to_goal = sqrt((x_goal - pos.x)*(x_goal - pos.x) + (y_goal - pos.y)*(y_goal - pos.y));

		const double linear_accel = 0.2, max_linear_speed = 1.0;
		const double turn_accel = 0.2, max_turn_speed = 1.0, min_turn_speed = 0.2;

	    geometry_msgs::Twist base_cmd;

	    printf("(%.2f, %.2f, %d deg) -> (%.2f, %.2f, %d deg)\n",
	    		pos.x, pos.y, int(pos.z * 180/M_PI), x_goal, y_goal, int(goal_angle * 180/M_PI));
	    if (distance_to_goal < 0.1 ||
	    	(linear_speed == 0 && distance_to_goal < 0.5)) {
	    	printf("Next point\n");
			// we are here, go to next point
			++current_path_count;
			return;
	    } else if (fabs(angle_to_goal) >= 0.2 ||
			(fabs(angle_to_goal) >= 0.05 && turn_speed != 0)) {
			// try to stop moving forward
			if (linear_speed <= linear_accel) {
				printf("Turning %d deg\n", int(angle_to_goal * 180/M_PI));
				linear_speed = 0;

				// now start (or continue) turning
				double sign = angle_to_goal > 0? 1 : -1;
				turn_speed += turn_accel * sign;
				if (fabs(turn_speed) > max_turn_speed) {
					turn_speed = max_turn_speed * sign;
				}
				if (fabs(turn_speed) > fabs(angle_to_goal)) {
					turn_speed = angle_to_goal;
				}
				if (fabs(turn_speed) < min_turn_speed) {
					turn_speed = min_turn_speed * sign;
				}
				base_cmd.angular.z = turn_speed;
			} else {
				// too fast, slow down first
				linear_speed -= linear_accel;
				base_cmd.linear.x = linear_speed;
			}
		} else {
			// try to stop turning
			if (fabs(turn_speed) <= turn_accel) {
				printf("Driving %.2f m\n", distance_to_goal);
				turn_speed = 0;

				// now start (or continue) driving
				linear_speed += turn_accel;
				if (linear_speed > max_linear_speed) {
					linear_speed = max_linear_speed;
				}
				if (linear_speed > distance_to_goal) {
					linear_speed = distance_to_goal;
				}
				base_cmd.linear.x = linear_speed;
			} else {
				// too fast, slow down first
				if (turn_speed < 0) turn_speed += turn_accel;
				else                turn_speed -= turn_accel;

				base_cmd.angular.z = turn_speed;
			}
		}

		if(obstacleInFront && linear_speed > 0)
		{
			printf("EMERGENCY STOP\n");
			base_cmd.linear.x = base_cmd.linear.y = base_cmd.angular.z = 0;
		}
		cmd_vel_pub_.publish(base_cmd);

	}


private:
	static double normalize_angle(double a) {
		while(a < -M_PI) a += 2*M_PI;
		while(a >= M_PI) a -= 2*M_PI;
		return a;
	}

  //Based on code from
  bool driveForwardOdom(double distance)
    {
      // If the distance to travel is negligble, don't even try.
      if (fabs(distance) < 0.01)
        return true;
      if(obstacleInFront){ //Laser obstacle check)
		 ROS_INFO("Can't drive forward. Obstacle detected");
    	  return false;
      }
      //we will record transforms here
      tf::StampedTransform start_transform;
      tf::StampedTransform current_transform;

      try
      {
        //wait for the listener to get the first message
        listener_.waitForTransform(base_frame, odom_frame,
                                   ros::Time::now(), ros::Duration(1.0));

        //record the starting transform from the odometry to the base frame
        listener_.lookupTransform(base_frame, odom_frame,
                                  ros::Time(0), start_transform);
      }
      catch (tf::TransformException ex)
      {
        ROS_ERROR("%s",ex.what());
        return false;
      }

      //we will be sending commands of type "twist"
      geometry_msgs::Twist base_cmd;
      //the command will be to go forward at 0.25 m/s
      base_cmd.linear.y = base_cmd.angular.z = 0;
      base_cmd.linear.x = forward_rate;

      if (distance < 0)
        base_cmd.linear.x = -base_cmd.linear.x;

      ros::Rate rate(25.0);
      bool done = false;
      while (!done && nh_.ok())
      {
        //send the drive command
        cmd_vel_pub_.publish(base_cmd);
        rate.sleep();
        //get the current transform
        try
        {
          listener_.lookupTransform(base_frame, odom_frame,
                                    ros::Time(0), current_transform);
        }
        catch (tf::TransformException ex)
        {
          ROS_ERROR("%s",ex.what());
          break;
        }
        //see how far we've traveled
        tf::Transform relative_transform =
          start_transform.inverse() * current_transform;
        double dist_moved = relative_transform.getOrigin().length();

        // Update feedback and result.
        feedback_forward = dist_moved;
        result_forward = dist_moved;
        //as_.publishFeedback(feedback_);

        if(fabs(dist_moved) > fabs(distance))
        {
          done = true;
        }
      }
      base_cmd.linear.x = 0.0;
      base_cmd.angular.z = 0.0;
      cmd_vel_pub_.publish(base_cmd);

      if (done) return true;
      return false;
    }

public:
  bool turnOdom(double radians)
    {
      // If the distance to travel is negligble, don't even try.
      if (fabs(radians) < 0.01)
        return true;

      while(radians < -M_PI) radians += 2*M_PI;
      while(radians > M_PI) radians -= 2*M_PI;

      //we will record transforms here
      tf::StampedTransform start_transform;
      tf::StampedTransform current_transform;

      try
      {
        //wait for the listener to get the first message
        listener_.waitForTransform(base_frame, odom_frame,
                                   ros::Time::now(), ros::Duration(1.0));

        //record the starting transform from the odometry to the base frame
        listener_.lookupTransform(base_frame, odom_frame,
                                  ros::Time(0), start_transform);
      }
      catch (tf::TransformException ex)
      {
        ROS_ERROR("%s",ex.what());
        return false;
      }

      //we will be sending commands of type "twist"
      geometry_msgs::Twist base_cmd;
      //the command will be to turn at 0.75 rad/s
      base_cmd.linear.x = base_cmd.linear.y = 0.0;
      base_cmd.angular.z = turn_rate;
      if (radians < 0)
        base_cmd.angular.z = -turn_rate;

      //the axis we want to be rotating by
      tf::Vector3 desired_turn_axis(0,0,1);

      ros::Rate rate(25.0);
      bool done = false;
      while (!done && nh_.ok())
      {
        //send the drive command
        cmd_vel_pub_.publish(base_cmd);
        rate.sleep();
        //get the current transform
        try
        {
          listener_.lookupTransform(base_frame, odom_frame,
                                    ros::Time(0), current_transform);
        }
        catch (tf::TransformException ex)
        {
          ROS_ERROR("%s",ex.what());
          break;
        }
        tf::Transform relative_transform =
          start_transform.inverse() * current_transform;
        tf::Vector3 actual_turn_axis =
          relative_transform.getRotation().getAxis();
        double angle_turned = relative_transform.getRotation().getAngle();

        // Update feedback and result.
        feedback_turn_distance = angle_turned;
        result_turn_distance = angle_turned;


        if ( fabs(angle_turned) < 1.0e-2) continue;

        //if ( actual_turn_axis.dot( desired_turn_axis ) < 0 )
        //  angle_turned = 2 * M_PI - angle_turned;

        if (fabs(angle_turned) > fabs(radians)) done = true;
      }
      if (done) return true;
      return false;
    }

private:
};


int main(int argc, char *argv[]) {
		goForward = false;
		ros::init(argc, argv, "navigator");
		ros::NodeHandle nh_;
		ros::Rate loop_rate(10);
		RobotDriver driver(nh_);
		LaserObsDetect laser(nh_);  //Sets a node level obstacleInFront variable to stop the drive
		while (nh_.ok()) {
			//ROS_INFO("loop");
			driver.drive();
			ros::spinOnce(); //the spinOnce/sleep lets the Laser catch up and flag whether an obstacle is in front
			loop_rate.sleep();
		}
		// Publishers
		//ros::spin();
		return 0;
}





