#include <string>
#include <algorithm>
#include <ros/ros.h>
#include <cmath>
#include <sensor_msgs/LaserScan.h> 
#include <geometry_msgs/Twist.h>
#include <geometry_msgs/PoseWithCovarianceStamped.h>
#include <tf/transform_listener.h>
#include <assign1_2013/path.h>
#include <assign1_2013/beacons.h>
#include <kudosu/Beacons.h>
#include "navigator.h"
#include <opencv2/imgproc/imgproc.hpp>
#include "opencv2/video/tracking.hpp"
#include <nav_msgs/Odometry.h>
 
using namespace std;

using geometry_msgs::Point;
bool obstacleInFront;
bool goForward;
b_pos world_beacons[6]; //The real world position of each beacon which matches our beacon_pos.which

sensor_msgs::LaserScan stored_Laser_Scan;
bool haveLaserScan = false;

const unsigned beacon_colours[6][2] = {
		{ 3, 0 }, // yellow/pink
		{ 0, 3 }, // pink/yellow
		{ 1, 0 }, // blue/pink
		{ 0, 1 }, // pink/blue
		{ 2, 0 }, // green/pink
		{ 0, 2 }, // pink/green
		};
const string colour_names[4] = { "pink", "blue", "green", "yellow" };


class LaserObsDetect {

	ros::Subscriber subLaserScan_;
	//ros::Publisher cmd_vel_pub_;

/*The LaserScan will set this flag to true when it detects no obstacles ahead.
The velocity publisher should not publish any movement messages in the false state;*/
public:
	LaserObsDetect(ros::NodeHandle nh)  {
		//subLaserScan_ = nh_.subscribe<sensor_msgs::LaserScan>("/scan",1,processLaserScan,this);
		//cmd_vel_pub_ = nh.advertise<geometry_msgs::Twist>("/mobile_base/commands/velocity", 1);
		subLaserScan_ = nh.subscribe("/scan", 1, &LaserObsDetect::processLaserScan, this);
		obstacleInFront=true;
	}

	~LaserObsDetect() {
	}

public:
	void processLaserScan(const sensor_msgs::LaserScan &laserScan) {
		stored_Laser_Scan = laserScan; //Store the laser to try and use with nav distance estimate
		haveLaserScan = true;
		float currentAngle = laserScan.angle_min;
		int angleIndex = 0;
		int numPoints = 0;
		float mindistanceInFront = 100; //distance the the minimal object in front of us.  Pretends its 100 until the laser finds something close
		float minxPt = 10;
		float minyPt = 10;
		double thresholdDistance = 0.4; //the closest distance in front the robot will go to an object
		double x_threshold = 0.2; //we use trig to make a "box" in front of the laser to detect obstacles that might hit the edges of our robot
		double angleRange = 0.52; //in radians
		while (currentAngle <= laserScan.angle_max) {
			float distance = laserScan.ranges[angleIndex];
			//if(currentAngle < angleRange && currentAngle > -angleRange){	

			// See if an obstacle was detected
			if (distance > laserScan.range_min && distance < laserScan.range_max) {
				float yPt = cosf(currentAngle) * distance; //This is the distance directly ahead displaced by xPt
				float xPt = sinf(currentAngle) * distance;
				if (abs(xPt) < x_threshold && abs(yPt) < thresholdDistance
						&& mindistanceInFront > distance) {
					//set that the obstacle was detected and at what angle
					mindistanceInFront = distance;
					minxPt = xPt;
					minyPt = yPt;
				}
			}
			//}
			angleIndex++;
			currentAngle += laserScan.angle_increment;

		}
		//If mindistanceInFront < 100 at this point, it means we detected something close than our minimum range


		if (mindistanceInFront == 100 && minxPt == 10 && minyPt == 10){
			//ROS_INFO("No obstacles immediately ahead");
			obstacleInFront = false;
		}
		else{
			//ROS_INFO("Detect obstacle %f at coords %f %f", mindistanceInFront, minxPt, minyPt);
			obstacleInFront = true;
		}


	}

};

class RobotDriver {
private:
	//! The node handle we'll be using
	ros::NodeHandle nh_;
	//! We will be publishing to the "/base_controller/command" topic to issue commands
	ros::Publisher cmd_vel_pub_;
	//ros::Subscriber pos_ekf_sub;
	ros::Subscriber beacon_sub;

	tf::TransformListener listener_;
	double turn_rate, forward_rate, feedback_forward, result_forward, feedback_turn_distance, result_turn_distance;
	std::string base_frame, odom_frame;
	comp3431::Path path;

	// current waypoint
	int current_path_count;
	// most recent localisation
	ros::Time last_localisation;
	// position at most recent localisation
	Point last_localisation_pos;
	bool checking_localisation;

	Point prev_odom;
	// offset to initial (arbitrary) odom position
	Point offset;
	bool have_offset;
	// drift in odom over time
	Point odom_drift;
	cv::KalmanFilter KF;
	// current driving speed
	double turn_speed, linear_speed;

	ros::Publisher pub_vo;

	comp3431::Beacons given_beacons;

	// time delay beacon detection
	beacon_pos prev_beacon;
	bool prev_beacon_valid;

	// remember beacons for debug printing
	char beacon_str[99];

	int frame;

	vector<ros::Time> last_time_beacon;

public:
	//! ROS node initialization
	RobotDriver(ros::NodeHandle &nh) {
		frame = 0;
		// init with impossible values
		last_localisation = ros::TIME_MIN;
		last_localisation_pos.x = 12345;
		last_localisation_pos.y = 12345;
		checking_localisation = false;

		nh_ = nh;
		//set up the publisher for the cmd_vel topic
		cmd_vel_pub_ = nh_.advertise<geometry_msgs::Twist>("/cmd_vel", 1);
		beacon_sub = nh_.subscribe<kudosu::Beacons>("/nav_beacons", 1, &RobotDriver::recv_beacons, this);
		//pos_ekf_sub = nh_.subscribe("/robot_pose_ekf/odom",1, &RobotDriver::drive, this); //switch to odom_combined
		base_frame = "base_link";
		odom_frame = "odom";
		turn_rate = 0.3;
		forward_rate = 0.5;

		feedback_forward = 0.0;
		result_forward = 0.0;
		current_path_count = 0;

		turn_speed = 0;
		linear_speed = 0;
		have_offset = false;
		pub_vo = nh_.advertise<nav_msgs::Odometry>("/vo", 0); //check this is correct
		last_time_beacon.resize(6, ros::TIME_MIN);

		KF.init(6, 3, 0); //x, y, yaw, x_vel, y_vel, yaw_vel <- dynamic params //x, y, yaw <- measurement
		KF.transitionMatrix = *(cv::Mat_<float>(6, 6) <<
				1,0,0,1,0,0,
				0,1,0,0,1,0,
				0,0,1,0,0,1,
				0,0,0,1,0,0,
				0,0,0,0,1,0,
				0,0,0,0,0,1);

		KF.statePost.setTo(0); //init position 0,0
		setIdentity(KF.measurementMatrix);
		setIdentity(KF.processNoiseCov, cv::Scalar::all(1e-3)); //how much jump between measures
		setIdentity(KF.measurementNoiseCov, cv::Scalar::all(1e-1)); //how much to trust measures
		setIdentity(KF.errorCovPost, cv::Scalar::all(.1));
		loadBeaconsFromFile();

		prev_beacon_valid = false;
		*beacon_str = '\0';
	}

	/*Apply our stored offset value to the raw Odometry data*/
	static Point add_offset(Point p, Point const offset) {
		double r = sqrt(p.x*p.x + p.y*p.y); //magnitude
		double a = atan2(p.y, p.x);
		p.x = r * cos(a + offset.z);
		p.y = r * sin(a + offset.z);
		p.x += offset.x;
		p.y += offset.y;
		p.z = normalize_angle(p.z + offset.z);
		return p;
	}

	/*The reverse transform for add_offset*/
	static Point remove_offset(Point p, Point const offset) {
		p.x -= offset.x;
		p.y -= offset.y;
		p.z = normalize_angle(p.z - offset.z);
		double r = sqrt(p.x*p.x + p.y*p.y);
		double a = atan2(p.y, p.x);
		p.x = r * cos(a - offset.z);
		p.y = r * sin(a - offset.z);
		return p;
	}

	/*Callback for our listener of our vo node.  The vo node reports the positions of each beacon it can currently detect
	 * from the camera.*/
	void recv_beacons(kudosu::Beacons beacons) {
		vector <beacon_pos> dists;
		//printf("recv_beacons\n");
		*beacon_str = '\0';
		ros::Time now = ros::Time::now();
		for (unsigned i = 0; i < 6; ++i) {
			if (beacons.beacons[i].z >= 0) {
				beacon_pos bp;

				bp.distance = beacons.beacons[i].x;
				bp.angleFi = beacons.beacons[i].y;
				bp.which = beacons.beacons[i].z;
				dists.push_back(bp);
				char buf[99];
				sprintf(buf, "[%6s/%6s (%5.2f m %3d deg)] ",
						colour_names[beacon_colours[bp.which][0]].c_str(),
						colour_names[beacon_colours[bp.which][1]].c_str(),
						bp.distance, int(bp.angleFi * 180/M_PI));
				strcat(beacon_str, buf);
				last_time_beacon.at(bp.which) = now;
				/*
				printf("\nBeacon %d (%.2f, %.2f) at %.2f m, %d deg\n",
						bp.which, world_beacons[bp.which].x, world_beacons[bp.which].y,
						bp.distance, int(bp.angleFi * 180/M_PI));
				*/
			}
		}
		Point current_pos = add_offset(prev_odom, odom_drift); //Correct from our recorded error
		Point robot_pos = position_robot(current_pos, dists); //calculate based on beacons we see
		/*if (robot_pos.x != current_pos.x || robot_pos.y != current_pos.y || robot_pos.z != current_pos.z) {
			printf("==> Position = (%.2f, %.2f, %d)\n", robot_pos.x, robot_pos.y, int(robot_pos.z*180/M_PI));
		}*/

		//printf("New drift (%.2f, %.2f, %.2f)", odom_drift.x, odom_drift.y, odom_drift.z);
		//Update the drift error offset
		odom_drift.z = robot_pos.z - prev_odom.z;
		double r = sqrt(prev_odom.x*prev_odom.x + prev_odom.y*prev_odom.y);
		double a = atan2(prev_odom.y, prev_odom.x);
		odom_drift.x = robot_pos.x - r * cos(-odom_drift.z - a);
		odom_drift.y = robot_pos.y + r * sin(-odom_drift.z - a);
		/*
		printf(" ==> (%.2f, %.2f, %d)  [%.2f %.2f %d]\n",
				odom_drift.x, odom_drift.y, int(odom_drift.z*180/M_PI),
				prev_odom.x, prev_odom.y, int(prev_odom.z*180/M_PI));
		*/

	}

	ros::Time lastTwoBeaconsSeen(){
		vector <ros::Time> tmp = last_time_beacon;
		sort(tmp.begin(), tmp.end());
		return tmp[tmp.size()-2];
	}

	/*loadBeaconsFromFile - Loads the beacon positions into our array based on Matt's input file
	 * This is for convenience as we identify each beacon by an int*/
	void loadBeaconsFromFile() {
		int num_beacons = given_beacons.beacons.size();
		if (!num_beacons) {
			ROS_INFO("No beacons are loaded");
			return;
		}
		for (int i = 0; i < num_beacons; i++) {
			comp3431::Beacon beac = given_beacons.beacons[i];
			if (beac.top == "yellow" && beac.bottom == "pink") {
				world_beacons[0].x = beac.position.x;
				world_beacons[0].y = beac.position.y;
			} else if (beac.top == "pink" && beac.bottom == "yellow") {
				world_beacons[1].x = beac.position.x;
				world_beacons[1].y = beac.position.y;
			} else if (beac.top == "blue" && beac.bottom == "pink") {
				world_beacons[2].x = beac.position.x;
				world_beacons[2].y = beac.position.y;
			} else if (beac.top == "pink" && beac.bottom == "blue") {
				world_beacons[3].x = beac.position.x;
				world_beacons[3].y = beac.position.y;
			} else if (beac.top == "green" && beac.bottom == "pink") {
				world_beacons[4].x = beac.position.x;
				world_beacons[4].y = beac.position.y;
			} else if (beac.top == "pink" && beac.bottom == "green") {
				world_beacons[5].x = beac.position.x;
				world_beacons[5].y = beac.position.y;
			}
			ROS_INFO("Loaded %f %f", beac.position.x, beac.position.y);
		}
	}


	// Localisation code - helper for our sort of beacons into left to right ordering.
	struct order_by_angle {
		bool operator()(beacon_pos const& a, beacon_pos const& b) const {
			return a.angleFi > b.angleFi;
		}
	};

	//Attempts to estimate the distance to the beacon from laser data
	//Not used as could not reliably filter out false positives (e.g. a wall close to a beacon or an
	//obstacle close to a beacon.  Also if our angle is wrong we miss the beacon entirely.
	//Attempts to estimate the distance to the beacon from laser data
	static beacon_pos guess_distance_at_angle_from_laser(beacon_pos beacon) {
		//printf("LASER\n");
		if(!haveLaserScan)
			return beacon;
		float currentAngle = stored_Laser_Scan.angle_min;
		int angleIndex = 0;
		int numPoints = 0;
		double minDist = 1000.0, maxDist = 0.0;
		double cAngle = 0.0;
		double angle_tolerance = 30;

		while (currentAngle <= stored_Laser_Scan.angle_max) {
			float distance = stored_Laser_Scan.ranges[angleIndex];
			//printf("%f\n", distance);
			if (distance > 0.01 && fabs(beacon.angleFi - currentAngle) < stored_Laser_Scan.angle_increment*angle_tolerance) {
				if (distance >= 0.85 * beacon.distance
						&& distance <= 1.15 * beacon.distance) {
					//possible find new distance
					beacon.distance = distance;
					printf("\nFound Scan - %s/%s Angle %f, distance %f\n",
							colour_names[beacon_colours[beacon.which][0]].c_str(),colour_names[beacon_colours[beacon.which][1]].c_str()
							, cAngle, distance);
					return beacon;
					//possDist = distance;
				}
			}
			angleIndex++;
			currentAngle += stored_Laser_Scan.angle_increment;
		}
		/*if(maxDist - minDist > 0.2){ //we need the beacon to stand our in the arc to know we have a "hit"
		 printf("Angle %f\n",cAngle);
		 return minDist; //we guess the closest object in our directed arc is the beacon
		 }*/
		return beacon; //return a negative number to indicate we didn't get a good match for a beacon
		//If mindistanceInFront < 100 at this point, it means we detected something close than our minimum range
	}

	/*Main localisation logic to position the robot.*/
	Point position_robot(Point cur_pos, vector <beacon_pos> beacons) {
		double x, y;
		if (beacons.size() == 0) {
			return cur_pos;
		}
		beacon_pos b1, b2;
		bool two_beacon = false;
		if (prev_beacon_valid && beacons.size() == 1 && beacons[0].which != prev_beacon.which) {
			// we only see one beacon, but can use a previously remembered beacon
			// we will use b1's position and angle, so set it to the up-to-date beacon
			b1 = beacons[0];
			// we will use b2's position only, because we rotated away from prev_beacon
			b2 = prev_beacon;

			//b1 = guess_distance_at_angle_from_laser(b1);
			//b2 = guess_distance_at_angle_from_laser(b2);
			two_beacon = true;

			// more debug
			char buf[99];
			sprintf(buf, "+%6s/%6s (%5.2f m %3d deg)+ ",
					colour_names[beacon_colours[b2.which][0]].c_str(),
					colour_names[beacon_colours[b2.which][1]].c_str(),
					b2.distance, int(b2.angleFi * 180/M_PI));
			strcat(beacon_str, buf);
		} else if (beacons.size() > 1) {
			// we see multiple beacons
			sort(beacons.begin(), beacons.end(), order_by_angle());
			b1 = beacons[0];
			b2 = beacons.back();
			//b1 = guess_distance_at_angle_from_laser(b1);
			//b2 = guess_distance_at_angle_from_laser(b2);
			two_beacon = true;
		}

		// update the prev_beacon
		if (!beacons.empty()) {
			prev_beacon = beacons[0];
			prev_beacon_valid = true; // for next frame
		}

		if (two_beacon) {
			// we have two beacons, calculate the correct position
			//printf("Single beacon estimate pt(%f, %f) %f degree\n",rp.x, rp.y, rp.z*180/M_PI);
			//printf("Laser guesses the beacon is at %f\n", guess_distance_at_angle_from_laser(beacons[0]));
			b_pos bp1 = world_beacons[b1.which], bp2 = world_beacons[b2.which];
			vector <Point> intersect = circle_circle_intersection(bp1.x, bp1.y, b1.distance, bp2.x, bp2.y, b2.distance);
			if (!intersect.empty()) {
				if (intersect.size() == 2) {
					// guess which intersection is correct
					double dist1 = dist(intersect[0].x - cur_pos.x, intersect[0].y - cur_pos.y);
					double dist2 = dist(intersect[1].x - cur_pos.x, intersect[1].y - cur_pos.y);
					if (dist1 < dist2) {
						cur_pos = intersect[0];
					} else {
						cur_pos = intersect[1];
					}
				} else if (intersect.size() == 1) {
					cur_pos.x = intersect[0].x;
					cur_pos.y = intersect[0].y;
				}
				cur_pos.z = atan2(bp1.y - cur_pos.y, bp1.x - cur_pos.x) + b1.angleFi;

				// this localisation is reliable
				last_localisation = ros::Time::now();
				last_localisation_pos = cur_pos;
				checking_localisation = false;
				return cur_pos;
			} else {
				printf("\n2 beacons intersection failed\n");
				return cur_pos;
			}
		} else if (beacons.size() == 1) {
			// we have only one beacon, try to make the most of it
			// we try to estimate our bearings with it, because the turtlebot angular odom sucks

			/*Circle intersect to closest point is not good.  We will only update our
			 * bearing for the single beacon case*/
			b_pos bpos = world_beacons[beacons[0].which];
			double dx = bpos.x - cur_pos.x, dy = bpos.y - cur_pos.y;
			double angle_offset = atan2(dy, dx);
			//calculate angle of the vector from the robot's current estimated position to the beacon
			//then add the angle of the beacon in the robots view
			double abs_bearing = angle_offset + beacons[0].angleFi;
			/*printf("From beacon (%.2f, %.2f) Abs_angle: %d dgs\n",
					dx, dy, int(abs_bearing * 180/M_PI));*/
			cur_pos.z = abs_bearing;
			return cur_pos;
		}

		// no beacons, could not do any localisation
		return cur_pos;
	}

	//closest_point_to_circle, Takes the centre of the circle, a radius and a point.  Returns the closest point on the circle
#if 0 // unused?
	static void closest_point_to_circle(double cX, double cY, double R, double pX, double pY, double *aX, double *aY){
		double vX = pX - cX;
		double vY = pY - cY;
		double magV = sqrt(vX*vX + vY*vY);
		*aX = cX + vX / magV * R;
		*aY = cY + vY / magV * R;

	}
#endif

	static vector <Point> circle_circle_intersection(
			double x0, double y0, double r0,
			double x1, double y1, double r1) { //, int b1, int b2
		// Soccer field size
		double max_x = 3.0;
		double max_y = 4.5;

		vector <Point> result;

		double a, dx, dy, d, h, rx, ry;
		double x2, y2;
		double xi, yi, xi_prime, yi_prime;

		dx = x1 - x0;
		dy = y1 - y0;

		/* Determine the straight-line distance between the centers. */
		d = hypot(dx, dy);

		/* Check for solvability. */
		if (d > (r0 + r1)) {
			/* no solution. circles do not intersect. */
			if (d < (r0 + r1 + 0.42)) {
				// circles are close, keep trying anyway
				double new_r0 = r0 * d / (r0 + r1);
				double new_r1 = r1 * d / (r0 + r1);
				r0 = new_r0, r1 = new_r1;
			} else {
				printf("\nCircles no solution\n");
				return vector <Point> ();
			}
		}
		if (d < fabs(r0 - r1)) {
			/* no solution. one circle is contained in the other */
			printf("\nCircle inside other circle\n");
			return vector <Point> ();
		}

		/* Determine the distance from point 0 to point 2. */
		a = ((r0 * r0) - (r1 * r1) + (d * d)) / (2.0 * d);

		/* Determine the coordinates of point 2. */
		x2 = x0 + (dx * a / d);
		y2 = y0 + (dy * a / d);

		/* Determine the distance from point 2 to either of the
		 * intersection points.
		 */
		h = sqrt((r0 * r0) - (a * a));

		/* Now determine the offsets of the intersection points from
		 * point 2.
		 */
		rx = -dy * (h / d);
		ry = dx * (h / d);

		/* Determine the absolute intersection point(s). */
		xi = x2 + rx;
		xi_prime = x2 - rx;
		yi = y2 + ry;
		yi_prime = y2 - ry;

		Point p;
		p.x = xi, p.y = yi;
		result.push_back(p);
		if (fabs(rx) > 0.1 || fabs(ry) > 0.1) {
			p.x = xi_prime, p.y = yi_prime;
			result.push_back(p); // second solution
		}
		return result;

		#if 0
		if (abs(xi) > max_x || abs(yi) > max_y) {
			if (abs(xi_prime) > max_x || abs(yi_prime) > max_y) {
				printf("\nCircles outside field\n");
				return result;
			} else {
				// obsolete
				*x = xi_prime;
				*y = yi_prime;
			}
		} else {
			if (abs(xi_prime) > max_x || abs(yi_prime) > max_y) {
				*x = xi;
				*y = yi;
			} else { // need to decide from what is left and right from your
				if (fabs(xi_prime - xi) < 0.01) {
					if ((yi + (yi_prime - yi) * (x0 - xi) / (xi_prime - xi)) > 0) {
						*x = xi;
						*y = yi;
					} else {
						*x = xi_prime;
						*y = yi_prime;
					}
				} else {
					if (yi_prime - yi > 0) {
						if (xi < x0) {
							*x = xi;
							*y = yi;
						} else {
							*x = xi_prime;
							*y = yi_prime;
						}
					} else {
						if (xi > x0) {
							*x = xi;
							*y = yi;
						} else {
							*x = xi_prime;
							*y = yi_prime;
						}
					}
				}
			}
		}

		return 1;
		#endif
	}


	//! Loop forever while sending drive commands based on keyboard input
public:
	void drive(){
		static ros::Time lastTime = ros::Time::now(); //onyl set on initialisation
		ros::Time currenttime = ros::Time::now(); //set every spin
		double dt = (currenttime - lastTime).toSec()*1000; //milliseconds
		lastTime = currenttime;

		/*Update velocity to do prediction correctly*/
		KF.transitionMatrix.at<float>(0,3) = dt;
		KF.transitionMatrix.at<float>(1,4) = dt;
		KF.transitionMatrix.at<float>(2,5) = dt;

		if(current_path_count >= path.points.size()){
			return;
		}

		tf::StampedTransform current_transform;
		//Get the latest position we know we are at
		try {
			listener_.waitForTransform(odom_frame, base_frame, ros::Time::now(),
					ros::Duration(1.0));
			listener_.lookupTransform(odom_frame, base_frame, ros::Time(0),
					current_transform);
		} catch (tf::TransformException const& ex) {
			ROS_ERROR("%s", ex.what());
			return;
		}

		Point pos; //Get current position
		pos.x = current_transform.getOrigin().getX();
		pos.y = current_transform.getOrigin().getY();
		pos.z = normalize_angle(tf::getYaw(current_transform.getRotation()));

		if(!have_offset) {
			offset = pos;
			printf("\nOffset = %f %f %f\n", offset.x, offset.y, offset.z);
			have_offset = true;
			return;
		} else {
			//printf("pos0 = %.2f %.2f %d   drift = %.2f %.2f %d\n",
			//		pos.x, pos.y, int(pos.z*180/M_PI), odom_drift.x, odom_drift.y, int(odom_drift.z*180/M_PI));
			pos = remove_offset(pos, offset);
			prev_odom = pos;
			pos = add_offset(pos, odom_drift);
		}

		// First predict, to update the internal statePre variable
		cv::Mat prediction = KF.predict(); //Moves estimate to where it thinks it should be
		//cv::Point predictPt(prediction.at<float>(0),prediction.at<float>(1));
		cv::Mat_<float> measurement(3,1); measurement.setTo(cv::Scalar(0));
		measurement(0) = pos.x;
		measurement(1) = pos.y;
		measurement(2) = pos.z;

		//cv::Vector<float> measPt(measurement(0),measurement(1),measurement(2));
		// The "correct" phase that is going to use the predicted value and our measurement
		cv::Mat estimated = KF.correct(measurement);
		//cv::Vector<float> statePt(estimated.at<float>(0),estimated.at<float>(1),estimated.at<float>(2));
		//printf("Before Kalman (x,y, angle) (%f, %f, %f)\n", pos.x, pos.y, pos.z);
		float k_x, k_y, k_z;
		k_x = estimated.at<float>(0);
		k_y = estimated.at<float>(1);
		k_z = estimated.at<float>(2);
		//printf("After Kalman (%f, %f, %f)\n", k_x, k_y, k_z);
		//Where is the next goal point
		geometry_msgs::Point current_goal = path.points[current_path_count];
		double x_goal = current_goal.x;
		double y_goal = current_goal.y;

		publish_visual_odometry(current_transform, k_x, k_y, k_z);
		pos.x = k_x;
		pos.y = k_y;
		pos.z = k_z;

		//work out angle to goal
		double goal_angle = atan2(y_goal - pos.y, x_goal - pos.x);
		double angle_to_goal = normalize_angle(goal_angle - pos.z);

		double distance_to_goal = sqrt((x_goal - pos.x)*(x_goal - pos.x) + (y_goal - pos.y)*(y_goal - pos.y));

		const double linear_accel = 0.1, max_linear_speed = .7;
		const double turn_accel = 0.1, max_turn_speed = .7, min_turn_speed = 0.1;

	    geometry_msgs::Twist base_cmd;

	    /*ros::Duration interval = ros::Time::now() - lastTwoBeaconsSeen();
	    if(interval.toSec() > 15){
	    	printf("\nAttempt to localise\n");
	    	angle_to_goal = 1; //cause robot to spin
	    }*/

		// Check our current position when:
		// - we are (seemingly) at a waypoint
		// - we have (seemingly) travelled more than 2m from the last localisation position
	    static char extra_msg[99] = {0};
	    static int extra_msg_frame = 0;
	    if (extra_msg_frame + 30 < frame) {
		*extra_msg = '\0';
	    }
	    if (distance_to_goal < 0.1) {
		// double check it
		ros::Duration d = ros::Time::now() - last_localisation;
		if (d.toSec() > 5) {
			sprintf(extra_msg, " (Checking waypoint %d)", current_path_count);
			extra_msg_frame = frame;
			angle_to_goal = 0.3; // cause robot to spin (slowly for better localisation)
			if (!checking_localisation) {
				checking_localisation = true;
				prev_beacon_valid = false; // discard cached data to improve recheck
			}
		} else {
			// should be correct, go to next waypoint
	    		++current_path_count;
			angle_to_goal = 0;
			distance_to_goal = 0;
		}
	    } else if (dist(last_localisation_pos.x - pos.x, last_localisation_pos.y - pos.y) > 2) {
		sprintf(extra_msg, " (Periodic localisation)");
		extra_msg_frame = frame;
		angle_to_goal = 0.3; // cause robot to spin (slowly for better localisation)
		if (!checking_localisation) {
			checking_localisation = true;
			prev_beacon_valid = false; // discard cached data to improve recheck
		}
	    }

	    // keep a batch of messages on the same line
	    if (++frame % 30 == 0) {
	    	printf("\n");
	    }
	    int w = printf("\r(%5.2f,%5.2f %3d deg) -> (%5.2f,%5.2f %3d deg) %s%s",
	    		pos.x, pos.y, int(pos.z * 180/M_PI), x_goal, y_goal, int(goal_angle * 180/M_PI),
	    		beacon_str, extra_msg);
	    printf("%*s", 144 - w, ""); // print spaces to clear the previous line (in the same position)
	    if(*beacon_str){
	    	printf("\n");
	    }

	    if (fabs(angle_to_goal) >= 0.2 ||
			(fabs(angle_to_goal) >= 0.1 && turn_speed != 0)) {
			// try to stop moving forward
			if (linear_speed <= linear_accel) {
				//printf("Turning %d deg\n", int(angle_to_goal * 180/M_PI));
				linear_speed = 0;

				// now start (or continue) turning
				double sign = angle_to_goal > 0? 1 : -1;
				turn_speed += turn_accel * sign;
				if (fabs(turn_speed) > max_turn_speed) {
					turn_speed = max_turn_speed * sign;
				}
				if (fabs(turn_speed) > fabs(angle_to_goal)) {
					turn_speed = angle_to_goal;
				}
				if (fabs(turn_speed) < min_turn_speed) {
					turn_speed = min_turn_speed * sign;
				}
				base_cmd.angular.z = turn_speed;
			} else {
				// too fast, slow down first
				linear_speed -= linear_accel;
				base_cmd.linear.x = linear_speed;
			}
		} else {
			// try to stop turning
			if (fabs(turn_speed) <= turn_accel) {
				//printf("Driving - Distance to goal= %.2f m\n", distance_to_goal);
				turn_speed = 0;

				// now start (or continue) driving
				linear_speed += turn_accel;
				if (linear_speed > max_linear_speed) {
					linear_speed = max_linear_speed;
				}
				if (linear_speed > distance_to_goal) {
					linear_speed = distance_to_goal;
				}
				base_cmd.linear.x = linear_speed;
			} else {
				// too fast, slow down first
				if (turn_speed < 0) turn_speed += turn_accel;
				else                turn_speed -= turn_accel;

				base_cmd.angular.z = turn_speed;
			}
		}

		if(obstacleInFront && linear_speed > 0)
		{
			printf("\nEMERGENCY STOP\n");
			base_cmd.linear.x = base_cmd.linear.y = base_cmd.angular.z = 0;
		}
		if (base_cmd.linear.x) {
			// driving will invalidate the prev_beacon position
			prev_beacon_valid = false;
		}
		cmd_vel_pub_.publish(base_cmd);

	}

public:

	double dist(double x, double y) {
		return sqrt(x*x + y*y);
	}

	/*Make 8 45 degree turns to attempt to localise the robot's position*/
	/*Would be good to record the distances to the three closest beacons
	 * To trilineate in this step*/
	void localise(){
		int turnCount = 0;
		while(turnCount < 4){
			turnOdom(M_PI/4); //turn
			turnCount++;
			ros::Duration(2).sleep(); //sleep to allow VO to process
		}
	}

public:
  bool turnOdom(double radians)
    {
      // If the distance to travel is negligble, don't even try.
      if (fabs(radians) < 0.01)
        return true;

      while(radians < -M_PI) radians += 2*M_PI;
      while(radians > M_PI) radians -= 2*M_PI;

      //we will record transforms here
      tf::StampedTransform start_transform;
      tf::StampedTransform current_transform;

      try
      {
        //wait for the listener to get the first message
        listener_.waitForTransform(base_frame, odom_frame,
                                   ros::Time::now(), ros::Duration(1.0));

        //record the starting transform from the odometry to the base frame
        listener_.lookupTransform(base_frame, odom_frame,
                                  ros::Time(0), start_transform);
      }
      catch (tf::TransformException ex)
      {
        ROS_ERROR("%s",ex.what());
        return false;
      }

      //we will be sending commands of type "twist"
      geometry_msgs::Twist base_cmd;
      //the command will be to turn at 0.75 rad/s
      base_cmd.linear.x = base_cmd.linear.y = 0.0;
      base_cmd.angular.z = turn_rate;
      if (radians < 0)
        base_cmd.angular.z = -turn_rate;

      //the axis we want to be rotating by
      tf::Vector3 desired_turn_axis(0,0,1);

      ros::Rate rate(25.0);
      bool done = false;
      while (!done && nh_.ok())
      {
        //send the drive command
        cmd_vel_pub_.publish(base_cmd);
        rate.sleep();
        //get the current transform
        try
        {
          listener_.lookupTransform(base_frame, odom_frame,
                                    ros::Time(0), current_transform);
        }
        catch (tf::TransformException ex)
        {
          ROS_ERROR("%s",ex.what());
          break;
        }
        tf::Transform relative_transform =
          start_transform.inverse() * current_transform;
        tf::Vector3 actual_turn_axis =
          relative_transform.getRotation().getAxis();
        double angle_turned = relative_transform.getRotation().getAngle();

        // Update feedback and result.
        feedback_turn_distance = angle_turned;
        result_turn_distance = angle_turned;


        if ( fabs(angle_turned) < 1.0e-2) continue;

        //if ( actual_turn_axis.dot( desired_turn_axis ) < 0 )
        //  angle_turned = 2 * M_PI - angle_turned;

        if (fabs(angle_turned) > fabs(radians)) done = true;
      }
      if (done) return true;
      return false;
    }


private:
	static double normalize_angle(double a) {
		while(a < -M_PI) a += 2*M_PI;
		while(a >= M_PI) a -= 2*M_PI;
		return a;
	}

	/*Helper function to publish our visual odometry to /vo*/
	void publish_visual_odometry(const tf::StampedTransform msg,
			double x_pos, double y_pos, double heading) {
		static nav_msgs::Odometry odom_output;

		odom_output.pose.pose.position.x = x_pos;
		odom_output.pose.pose.position.y = y_pos;
		odom_output.pose.pose.orientation = tf::createQuaternionMsgFromYaw(
				heading);
		if (x_pos == 0)
			odom_output.pose.covariance[0] = 999999; //X-X
		else
			odom_output.pose.covariance[0] = 0.000001; //X-X
		if (y_pos == 0)
			odom_output.pose.covariance[7] = 999999; //Y-Y
		else
			odom_output.pose.covariance[7] = 0.000001; //Y-Y
		odom_output.pose.covariance[14] = 999999; //Z-Z
		odom_output.pose.covariance[21] = 999999; //Roll-Roll
		odom_output.pose.covariance[28] = 999999; //Pitch-Pitch
		if (heading == 0)
			odom_output.pose.covariance[35] = 999999; //Yaw-Yaw
		else
			odom_output.pose.covariance[35] = 0.000001; //Yaw-Yaw
		odom_output.twist.covariance = odom_output.pose.covariance;

		odom_output.header.stamp = msg.stamp_;
		odom_output.header.seq++;
		//Note: We assume we will never be at 0,0,0 exactly except at the start of the run
		//So if we do not have a reading for X, Y or Z, set its covariance high so it is ignored
		pub_vo.publish(odom_output);
	}

};

int main(int argc, char *argv[]) {
		goForward = false;
		ros::init(argc, argv, "kudosu_nav");
		ros::NodeHandle nh_;
		ros::Rate loop_rate(10);



		RobotDriver driver(nh_);
		LaserObsDetect laser(nh_);  //Sets a node level obstacleInFront variable to stop the drive
		while (nh_.ok()) {
			//ROS_INFO("loop");
			driver.drive();
			ros::spinOnce(); //the spinOnce/sleep lets the Laser catch up and flag whether an obstacle is in front
			loop_rate.sleep();
		}
		return 0;
}





