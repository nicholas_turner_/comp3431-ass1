#include <ros/ros.h>
#include <image_transport/image_transport.h>
#include <cv_bridge/cv_bridge.h>
#include <sensor_msgs/image_encodings.h>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <nav_msgs/Odometry.h>
#include <sstream>
#include <assign1_2013/beacons.h>
#include <pluginlib/class_list_macros.h>
#include <nodelet/nodelet.h>

using namespace cv;
using namespace std;
namespace enc = sensor_msgs::image_encodings;

namespace example_pkg {

// Beacon top/bottom colour combinations
	static const unsigned num_beacons = 6;
	static unsigned beacon_colours[num_beacons][2] = {
			{ 3, 0 }, // yellow/pink
			{ 0, 3 }, // pink/yellow
			{ 1, 0 }, // blue/pink
			{ 0, 1 }, // pink/blue
			{ 2, 0 }, // green/pink
			{ 0, 2 }, // pink/green
			};

	// Minimum and maximum values for each colour (HSV encoding)
	static const unsigned num_colours = 4;
	static const Scalar colours[num_colours][2] = { { Scalar(150, 50, 50), Scalar(170, 255,
			255) }, // pink
			{ Scalar(90, 50, 50), Scalar(110, 255, 255) }, // blue
			{ Scalar(70, 20, 20), Scalar(90, 255, 255) }, // green
			{ Scalar(20, 50, 50), Scalar(40, 255, 255) }, // yellow
			};
	// for debugging
	static const Scalar rgb_colours[4] = { Scalar(255, 0, 255), Scalar(255, 0, 0), Scalar(
			0, 255, 0), Scalar(0, 255, 255) };



	static const double max_x = 3.0; //Max_X
	static const double max_y = 4.5; //Max_Y
	static const double H = 20.0;

class MyNodeletClass: public nodelet::Nodelet {
	ros::NodeHandle nh_;
	struct example_pos {
			double x, y; // from centre of screen, range = -0.5 .. 0.5
			double h, w, factor; // height, factor for scaling
			double distanceX, distanceY, angleFi;// real distance X, Y and angle between head and beacon
		};


	struct beacons {
		//unsigned which; // which beacon (0 .. 5)
		double x, y;// from centre
	};

	//Convenience struct for robot position
	struct robot_pos {
		double x, y; // absolute x, y coordinate in the world
		double alpha; //bearing from x-axis+
		double distance; //distance to closest beacon
		double acc; //accuracy
	};

	// beacon position on camera screen
	struct beacon_pos {
		double x, y; // from centre of screen, range = -0.5 .. 0.5
		double w, h; // fraction of screen size, range 0 .. 1
		unsigned which; // which beacon (0 .. 5)
	};

	struct best_fit{
		double factor, angleFi;
	};
	struct dist_angle{
		double distance, angleFi;
	};

	struct b_pos{
		double x, y;
	};



	b_pos world_beacons[6]; //The real world position of each beacon which matches our beacon_pos.which

	ros::Publisher pub_vo;


	image_transport::Subscriber image_sub_;
	image_transport::Publisher image_pub_;
	image_transport::Publisher image_debug_;
	cv::Mat src, test2;
	comp3431::Beacons given_beacons; //TODO: Need to order these in the expected 0-5 ordering used for colour recognition
	int frame;



public:
	virtual void onInit();
public:
	virtual void imageCb(const sensor_msgs::ImageConstPtr& msg);
public:
	virtual double beaconScore(const Rect top, const Rect bot, const int screen_width,
				const int screen_height);
public:
	virtual vector<beacon_pos> findBeacons(Mat input, cv_bridge::CvImagePtr cv_ptr);
public:
	virtual void publish_visual_odometry(const sensor_msgs::ImageConstPtr& msg,
			int x_pos, int y_pos, int heading);
public:
	virtual int circle_circle_intersection(double x0, double y0, double r0,
			double x1, double y1, double r1, double *x, double *y);
	virtual best_fit find_best_fit(const beacon_pos pos);
	virtual dist_angle distance_from_beacon(const beacon_pos pos);
	virtual void order_beacons(vector<beacon_pos> beacons,
					vector<dist_angle> d_as);
	virtual	robot_pos position_robot(vector<beacon_pos> beacons);
};

void MyNodeletClass::onInit() {

		NODELET_DEBUG("Initializing nodelet...");
		ROS_INFO("Constructor");
		frame = 0;
		image_transport::ImageTransport it_(nh_);
		image_pub_ = it_.advertise("out", 1);
		image_debug_ = it_.advertise("/out_debug", 1);
		image_sub_ = it_.subscribe("/camera/rgb/image_color_throttle", 1, &MyNodeletClass::imageCb, this);
		pub_vo = nh_.advertise<nav_msgs::Odometry>("/vo", 0); //check this is correct
	}

void MyNodeletClass::imageCb(const sensor_msgs::ImageConstPtr& msg) {
		double const drop_frames = 0.0; // 90%
		++frame;
		if (int(frame * (1 - drop_frames))
				== int((frame - 1) * (1 - drop_frames))) {
			ROS_INFO("Frame (dropped)");
			return;
		} else {
			ROS_INFO("Frame (processed)");
		}

		cv_bridge::CvImagePtr cv_ptr;
		try {
			cv_ptr = cv_bridge::toCvCopy(msg, enc::BGR8);
		} catch (cv_bridge::Exception& e) {
			ROS_ERROR("cv_bridge exception: %s", e.what());
			return;
		}

		/*
		 if (cv_ptr->image.rows > 60 && cv_ptr->image.cols > 60)
		 cv::circle(cv_ptr->image, cv::Point(50, 50), 10, CV_RGB(255,0,0));
		 */

		cv_ptr->image.copyTo(src);
		// downsample from 640x480
		resize(src, src, Size(320, 240));

		vector<beacon_pos> beacons = findBeacons(src, cv_ptr);
		//ROS_INFO("Found %u beacons", unsigned(beacons.size()));
		const Scalar rgb_colours[4] = { Scalar(255, 0, 255), Scalar(255, 0, 0),
				Scalar(0, 255, 0), Scalar(0, 255, 255) };
		// Beacon top/bottom colour combinations
		const unsigned num_beacons = 6;
		unsigned beacon_colours[num_beacons][2] = { { 3, 0 }, // yellow/pink
				{ 0, 3 }, // pink/yellow
				{ 1, 0 }, // blue/pink
				{ 0, 1 }, // pink/blue
				{ 2, 0 }, // green/pink
				{ 0, 2 }, // pink/green
				};
		for (unsigned i = 0; i < beacons.size(); ++i) {
			{

				cv::Point c((beacons[i].x + 0.5) * src.cols,
						(beacons[i].y + 0.5 - beacons[i].h / 4) * src.rows);
				cv::circle(src, c, beacons[i].w / 2.0 * src.cols,
						rgb_colours[beacon_colours[beacons[i].which][0]], 3, 8,
						0);
				stringstream out, out2;
				out << "XY" << (beacons[i].x + 0.5) * src.cols << " "
						<< (beacons[i].y + 0.5) * src.rows << "HxW"
						<< beacons[i].h / 2.0 * src.cols << " "
						<< beacons[i].w / 2.0 * src.cols;
				//range = -0.5 .. 0.5
				cv::putText(src, out.str(), c + Point(1.0, 1.0),
						CV_FONT_HERSHEY_COMPLEX, 0.5, Scalar(255, 255, 0));
			}
			{
				cv::Point c((beacons[i].x + 0.5) * src.cols,
						(beacons[i].y + 0.5 + beacons[i].h / 4) * src.rows);
				cv::circle(src, c, beacons[i].w / 2.0 * src.cols,
						rgb_colours[beacon_colours[beacons[i].which][1]], 3, 8,
						0);
			}
		}
		robot_pos robotIsHere = position_robot(beacons);
				printf("Robot is here x:%f y:%f acc:%f alpha:%f distance:%f\n",
						robotIsHere.x, robotIsHere.y, robotIsHere.acc,
						robotIsHere.alpha, robotIsHere.distance);
		cv_ptr->image = src;
		image_pub_.publish(cv_ptr->toImageMsg());

#if 0
		Mat src_contoured;
		src.copyTo(src_contoured);
		int thresh = 30;
		std::vector<std::vector<cv::Point> > contours;
		std::vector<cv::Vec4i> hierarchy;
		cv::Canny(src_contoured, src_contoured, thresh, thresh * 2, 3);
		cv::findContours(src_contoured, contours, hierarchy, cv::RETR_EXTERNAL,
				cv::CHAIN_APPROX_NONE);

		/// Approximate contours to polygons + get bounding rects and circles
		vector<vector<Point> > contours_poly(contours.size());
		vector<Rect> boundRect(contours.size());
		vector<Point2f> center(contours.size());
		vector<float> radius(contours.size());

		//Exclude rectangles which are not within certain ratios
		int c = 0;
		for (int i = 0; i < contours.size(); i++) {
			//approxPolyDP(Mat(contours[i]), contours_poly[i], 3, true);
			//Rect rec = boundingRect(Mat(contours[i]));
			//int ratio = rec.height/rec.width;
			//if(ratio < 1.7 || ratio > 2.3)
			//	continue;
			boundRect[c++] = boundingRect(Mat(contours[i]));
			minEnclosingCircle((Mat) contours[i], center[i], radius[i]);
		}

		Mat drawing = Mat::zeros(src.size(), CV_8UC3);
		for (int i = 0; i < contours.size(); i++) {
			Scalar color = Scalar(rng.uniform(0, 255), rng.uniform(0, 255),
					rng.uniform(0, 255));
			//drawContours( drawing, contours, i, color, 2, 8, hierarchy, 0, Point() );
			rectangle(drawing, boundRect[i].tl(), boundRect[i].br(), color, 2,
					8, 0);
			ROS_INFO("Height %d, Width %d", boundRect[i].height, boundRect[i].width);
			circle(drawing, center[i], (int) radius[i], color, 2, 8, 0);
		}

		calculate_odometry();
		publish_visual_odometry(msg, 1, 2, 3);

		/*Output the image*/
		cv_ptr->image.copyTo(drawing, src);
		drawing.copyTo(cv_ptr->image);
		image_pub_.publish(cv_ptr->toImageMsg()); //We can remove this from the final product unless we want to watch the robot
#endif

	}



	double MyNodeletClass::beaconScore(const Rect top, const Rect bot, const int screen_width,
			const int screen_height) {
		// calculate "score"
		double score = 0;

		// aspect ratio of each rect (smaller is better)
		double aspect;
		aspect = double(top.height) / top.width;
		if (aspect < 1)
			aspect = 1 / aspect;
		if (aspect > 2)
			return 999;
		score += aspect - 1;
		aspect = double(bot.height) / bot.width;
		if (aspect < 1)
			aspect = 1 / aspect;
		if (aspect > 2)
			return 999;
		score += aspect - 1;

		// difference between top/bottom sizes (nearer is better)
		double top_sz = sqrt(top.width * top.height);
		double bot_sz = sqrt(bot.width * bot.height);
		aspect = top_sz / bot_sz;
		if (aspect < 1)
			aspect = 1 / aspect;
		if (aspect > 2)
			return 999;
		score += aspect - 1;

		// top/bottom separation (nearer is better)
		//double bla = top.x + top.width / 2.0;
		double top_mx = top.x + top.width / 2.0;
		double top_my = top.y + top.height;
		double bot_mx = bot.x + bot.width / 2.0;
		double bot_my = bot.y;
		double average_sz = sqrt(top_sz * bot_sz);
		double screen_sz = sqrt(screen_width * screen_height);
		double tmp = (top_mx - bot_mx) * (top_mx - bot_mx)
				+ (top_my - bot_my) * (top_my - bot_my);
		if (tmp < 0)
			tmp = 0;
		double dist = sqrt(tmp) / average_sz;
		if (dist > 0.5)
			return 999;
		score += dist;

		// size on screen (bigger is better)
		score -= average_sz / screen_sz;

		return score;
	}

	vector<MyNodeletClass::beacon_pos> MyNodeletClass::findBeacons(Mat input, cv_bridge::CvImagePtr cv_ptr) {

		Mat chan_hsv;
		//find contours
		std::vector<cv::Vec4i> hierarchy;
		Mat temp2_gray, canny_output;

		cv::cvtColor(input, chan_hsv, CV_BGR2HSV);

		// Minimum and maximum values for each colour (HSV encoding)
		const unsigned num_colours = 4;
		const Scalar colours[num_colours][2] = { { Scalar(150, 50, 50), Scalar(
				170, 255, 255) }, // pink
				{ Scalar(90, 50, 50), Scalar(110, 255, 255) }, // blue
				{ Scalar(70, 20, 20), Scalar(90, 255, 255) }, // green
				{ Scalar(20, 50, 50), Scalar(40, 255, 255) }, // yellow
				};
		// for debugging
		const Scalar rgb_colours[4] = { Scalar(255, 0, 255), Scalar(255, 0, 0),
				Scalar(0, 255, 0), Scalar(0, 255, 255) };

		// Beacon top/bottom colour combinations
		const unsigned num_beacons = 6;
		unsigned beacon_colours[num_beacons][2] = { { 3, 0 }, // yellow/pink
				{ 0, 3 }, // pink/yellow
				{ 1, 0 }, // blue/pink
				{ 0, 1 }, // pink/blue
				{ 2, 0 }, // green/pink
				{ 0, 2 }, // pink/green
				};

		// Recognise regions of each colour
		std::vector<std::vector<cv::Point> > contours[num_colours];
		std::vector<Rect> boundRect[num_colours];

		Mat input_;
		input.copyTo(input_);
		for (unsigned i = 0; i < num_colours; ++i) {
			Mat mask;
			// Mask out this colour
			cv::inRange(chan_hsv, colours[i][0], colours[i][1], mask);
			//Clean up the image - Remove noise
			cv::erode(mask, mask, cv::Mat(), cv::Point(-1, -1), 2);

			//Fill in the gaps
			cv::dilate(mask, mask, cv::Mat());
			cv::GaussianBlur(mask, mask, cv::Size(3, 3), 0, 0);

			// Find all blobs of this colour
			std::vector<cv::Vec4i> hierarchy;
			cv::findContours(mask, contours[i], hierarchy, cv::RETR_EXTERNAL,
					cv::CHAIN_APPROX_NONE);
			for (unsigned j = 0; j < contours[i].size(); ++j) {
				Rect br = boundingRect(Mat(contours[i][j]));

#if 0
				// discard blobs that are too small
				unsigned min_size = 10;
				if(br.width < min_size || br.height < min_size) {
					continue;
				}
#endif
				boundRect[i].push_back(br);
				rectangle(input_, br, rgb_colours[i], 2, 8, 0);
				//string text;
				//text << "H: " << br.height << " W: " << br.width << "topleft x: " << br.x << " topleft y: " << br.y;
			}

		}
		cv_ptr->image = input_;
		image_debug_.publish(cv_ptr->toImageMsg());

		// Find regions that look like beacons
		vector<beacon_pos> beacons;
		for (unsigned i = 0; i < num_beacons; ++i) {
			// Just brute-force it for now
			const unsigned top_colour = beacon_colours[i][0], bot_colour =
					beacon_colours[i][1];

			// Find the "best" pair of top and bottom colour regions
			unsigned best_top = 0, best_bot = 0;
			beacon_pos best_beacon;
			best_beacon.which = i;
			double best_score = numeric_limits<double>::infinity();

			for (unsigned j = 0; j < boundRect[top_colour].size(); ++j) {
				for (unsigned k = 0; k < boundRect[bot_colour].size(); ++k) {
					Rect top = boundRect[top_colour][j], bot =
							boundRect[bot_colour][k];
					double score = beaconScore(top, bot, input.cols,
							input.rows);

					if (!(score >= best_score)) {
						best_top = j;
						best_bot = k;
						best_score = score;
						double average_xcentre = (top.x + top.width / 2.0
								+ bot.x + bot.width / 2.0) / 2 / input.cols;
						best_beacon.x = average_xcentre - 0.5;
						double average_ycentre = double(
								top.y + (bot.y + bot.height)) / 2 / input.rows;
						best_beacon.y = average_ycentre - 0.5;
						best_beacon.w = (top.width + bot.width) / 2.0
								/ input.cols;
						best_beacon.h = double(top.height + bot.height)
								/ input.rows;
					}
				}
			}
			const double score_threshold = 1;
			//ROS_INFO("Beacon %u score = %f", i, best_score);
			if (best_score < score_threshold) {
				beacons.push_back(best_beacon);
			}
		}

		return beacons;

		//cv::Mat elements(5,5, CV_8U, cv::Scalar(1));

#if 0
		//Clean up the image - Remove noise
		//MORPH_CROSS; MORPH_ELLIPSE//MORPH_RECT
		int dilation_size = 0;
		cv::Mat element = getStructuringElement(cv::MORPH_CROSS,
				cv::Size(2 * dilation_size + 1, 2 * dilation_size + 1),
				cv::Point(dilation_size, dilation_size));
		cv::erode(combined_mask, combined_mask, cv::Mat(), cv::Point(-1,-1), 2);

		//Fill in the gaps
		cv::dilate(combined_mask, combined_mask, cv::Mat());
		cv::GaussianBlur(combined_mask, combined_mask, cv::Size(5,5), 0,0);

		//cv::morphologyEx(temp2, temp2, cv::MORPH_CLOSE, elements); //This is an erode and then a dilate
		input.copyTo(combined_mask, combined_mask);//copy the coloured image onto our mask
		combined_mask.copyTo(input);//copy our mask back to the Matrix which we passed in to this function
#endif
	}

	/*Change the signature if convenient*/
	void MyNodeletClass::publish_visual_odometry(const sensor_msgs::ImageConstPtr& msg,
			int x_pos, int y_pos, int heading) {
		static nav_msgs::Odometry odom_output;
		Mat covar_matrix;
		Mat quarternion;
		//I think twist is velocities so we may not want to use this. CHECK DOCUMENTATION
		//odom_output.twist.twist.angular.z = 1.0;
		//odom_output.twist.twist.linear.x = x;
		//odom_output.twist.twist.linear.y = y;
		//odom_output.twist.covariance = covar_matrix;

		odom_output.pose.pose.position.x = x_pos;
		odom_output.pose.pose.position.y = y_pos;
		//odom_output.pose.pose.position.z = x; //height
		//Our heading???
		odom_output.pose.pose.orientation.w = heading; //Dummy variable
		odom_output.pose.pose.orientation.x = heading;
		odom_output.pose.pose.orientation.y = heading;
		odom_output.pose.pose.orientation.z = heading;

		/*NEED TO ADD THE COVARIANCE MATRIX AS WELL*/

		odom_output.header.stamp = msg->header.stamp;
		odom_output.header.seq++;
		pub_vo.publish(odom_output);
	}

	int MyNodeletClass::circle_circle_intersection(double x0, double y0, double r0,
				double x1, double y1, double r1, double *x, double *y) { //, int b1, int b2
			double a, dx, dy, d, h, rx, ry;
			double x2, y2;
			double xi, yi, xi_prime, yi_prime;

			/* dx and dy are the vertical and horizontal distances between
			 * the circle centers.
			 */
			dx = x1 - x0;
			dy = y1 - y0;

			/* Determine the straight-line distance between the centers. */
			//d = sqrt((dy*dy) + (dx*dx));
			d = hypot(dx, dy); // Suggested by Keith Briggs

			/* Check for solvability. */
			if (d > (r0 + r1)) {
				/* no solution. circles do not intersect. */
				return 0;
			}
			if (d < fabs(r0 - r1)) {
				/* no solution. one circle is contained in the other */
				return 0;
			}

			/* 'point 2' is the point where the line through the circle
			 * intersection points crosses the line between the circle
			 * centers.
			 */

			/* Determine the distance from point 0 to point 2. */
			a = ((r0 * r0) - (r1 * r1) + (d * d)) / (2.0 * d);

			/* Determine the coordinates of point 2. */
			x2 = x0 + (dx * a / d);
			y2 = y0 + (dy * a / d);

			/* Determine the distance from point 2 to either of the
			 * intersection points.
			 */
			printf("%f\n", sqrt((r0 * r0) - (a * a)));
			h = sqrt((r0 * r0) - (a * a));

			/* Now determine the offsets of the intersection points from
			 * point 2.
			 */
			rx = -dy * (h / d);
			ry = dx * (h / d);

			/* Determine the absolute intersection points. */
			xi = x2 + rx;
			xi_prime = x2 - rx;
			yi = y2 + ry;
			yi_prime = y2 - ry;
			if (abs(xi) > max_x || abs(yi) > max_y) {
				if (abs(xi_prime) > max_x || abs(yi_prime) > max_y) {
					return (-1); //	both points are out of the field
				} else {
					*x = xi_prime;
					*y = yi_prime;
				}
			} else {
				if (abs(xi_prime) > max_x || abs(yi_prime) > max_y) {
					*x = xi;
					*y = yi;
				} else { // need to decide from what is left and right from your
					if (xi_prime - xi != 0) {
						if ((yi + (yi_prime - yi) * (x0 - xi) / (xi_prime - xi))
								> 0) {
							*x = xi;
							*y = yi;
						} else {
							*x = xi_prime;
							*y = yi_prime;
						}
					} else {
						if (yi_prime - yi > 0) {
							if (xi < x0) {
								*x = xi;
								*y = yi;
							} else {
								*x = xi_prime;
								*y = yi_prime;
							}
						} else {
							{
								if (xi > x0) {
									*x = xi;
									*y = yi;
								} else {
									*x = xi_prime;
									*y = yi_prime;
								}
							}
						}
					}
				}
			}

			return 1;
		}
	MyNodeletClass::best_fit MyNodeletClass::find_best_fit(const beacon_pos pos){
			example_pos test_pos[50]=
				{{0.0165,0.1125,0.279,0.051953125,
				0.93465,0,50,0}, //x,y, h, w, factor, dist_in_front (cm), dist_to_side, angle (radians)
				{0.0203,0.05708333333333,0.177,	0.03203125,
				1.035936,0,100,0},
				{0.021675,0.03264583333333,0.11667,	0.023828125,
				0.974169, 0, 150,0},
				{0.021675,0.02118020833333,0.06959,	0.01953125,
				0.75493,0, 200, 0},
				{0.026125,0.0125,	0.06675,0.01640625,
				0.891113, 0, 250, 0},
				{0.045313,	0.009375,0.06041,0.012890625,
				0.957594, 0, 300, 0},
				{0.270703,0.1135416667,0.077063,0.059375,
				1.031869, 30, 50,-0.5404195003},
				{0.16953,0.1114583333,0.104167,0.053125,
				0.992532,20,50,-0.3805063771},
				{0.2,0.109375,0.175,0.053125,
				1.062277, 10,50,-0.1973955598},
				{0.2745,0.0326458333,0.16125,0.02890625,
				1.00542, 75, 150,	-0.463647609},
				{0.299219,0.0132291667,0.318,0.016015625,
				1.159137,50,250,-0.1973955598},
				{0.33677,0.0114583333,0.161,0.01640625,
				1.036867,100,250,-0.3805063771},
				{0.314453,0.0583333333,0.2167,0.0296875,
				1.153307,10,100, -0.0996686525},
				{0.335547,0.0583333333,0.1166666667,0.0296875,
				1.17731,10,100, -0.0996686525}};

			best_fit bf;
			double hh, xh;
			bf.angleFi = 0.0;
			bf.factor = 0.702654;
			double min_ed = 2.0;
			double min_edh = 4.0;
			for (int i = 0; i < 14; i++) {
				if (abs(pos.h - test_pos[i].h) < min_edh) {
					bf.factor = test_pos[i].factor;
					min_ed = abs(pos.h - test_pos[i].h);
				}
				hh = abs(test_pos[i].h - pos.h);
				xh = abs(test_pos[i].x) - abs(pos.x);
				printf("%f\n", sqrt(xh * xh + hh * hh));
				if (sqrt(xh * xh + hh * hh) < min_ed) {
					//Approximate by the closest position from the data set
					// or can be by set of data which belong the same 'class'
					printf("%f\n", sqrt(xh * xh + hh * hh));
					min_ed = sqrt(xh * xh + hh * hh);
					if (test_pos[i].x * pos.x > 0) {
						bf.angleFi = test_pos[i].angleFi;
						bf.factor = test_pos[i].factor;
					} else {
						bf.angleFi = -(test_pos[i].angleFi);
						bf.factor = test_pos[i].factor;
					}
				}

			} //when you know distance and x you
			  //bf.factor=1.354478;
			return bf;
		}
	MyNodeletClass::dist_angle MyNodeletClass::distance_from_beacon(const beacon_pos pos) {
			// calculate "distance"
			// this procedure will ....
			//double distance = 0.0;
			dist_angle d_a;
			d_a.angleFi = 0.0;
			d_a.distance = 0.0;
			best_fit bf;
			bf = find_best_fit(pos);
			//double alpha=0.0;
			d_a.distance = bf.factor * H / pos.h;
			d_a.angleFi = bf.angleFi;
			return d_a;
		}

		void MyNodeletClass::order_beacons(vector<beacon_pos> beacons,
				vector<dist_angle> d_as) {
			//order beacons by angle that we have
			if (beacons.size() > 1) {
				beacon_pos bhelp;
				dist_angle d_ahelp;
				for (int i = 0; i < beacons.size() - 1; i++) {
					for (int j = i + 1; j < beacons.size(); j++) {
						if (d_as[i].angleFi < d_as[j].angleFi) {
							bhelp = beacons[j];
							beacons[j] = beacons[i];
							beacons[i] = bhelp;
							d_ahelp = d_as[j];
							d_as[j] = d_as[i];
							d_as[i] = d_ahelp;
						}
					}
				}
			}
		}
		MyNodeletClass::robot_pos MyNodeletClass::position_robot(vector<beacon_pos> beacons) {
			double x, y, xi_2, yi_2, b1_x, b1_y, b2_x, b2_y, distance, alpha;
			robot_pos rp;
			rp.x = 12.34;
			rp.y = 12.34;
			rp.alpha = 12.34;
			rp.acc = 12.34;
			rp.distance = 12.34;
			dist_angle d_a;
			//dist_angle d_as[6];
			vector <dist_angle> d_as;
			//b_pos bp[6]={{-3,0},{3,0},{-3,4.5},{3,4.5},{-3,-4.5},{3,-4.5}};
			if (beacons.size() == 0) {
				rp.x = 0.0;
				rp.y = 0.0;
				rp.alpha = 0.0;
				rp.acc = 0.0;
				rp.distance = 0.0;
				printf("No beacons %f %f\n", rp.x, rp.y);
				return rp;
			}
			if (beacons.size() == 1) {
				rp.x = 0.0;
				rp.y = 0.0;
				d_a.distance = 0.0;
				d_a.angleFi = 0.0;
				d_a = distance_from_beacon(beacons[0]);

				rp.distance = d_a.distance;
				rp.alpha = d_a.angleFi;
				rp.acc = 0;
				printf("1 beacon %f %f\n", rp.x, rp.y);
				return rp;
			}
			if (beacons.size() > 1) {
				for (unsigned i = 0; i < beacons.size(); ++i) {
					d_as.push_back(distance_from_beacon(beacons[i]));
				}
				/*for (unsigned i = 0; i < beacons.size(); ++i) {
				 d_as[i] = distance_from_beacon(beacons[i]);
				 }*/
				// use 2 beacons to calculate distance from them and use the intersecion of the circles
				// to find x and y. The position of the baecons will give us which x and y to choose from 2 of them.
				// If you have the third distance (beacon), use it to check things that you get previously
				if (beacons.size() == 2) {
					beacon_pos bhelp;
					dist_angle d_ahelp;
					for (int i = 0; i < beacons.size() - 1; i++) {
						for (int j = i + 1; j < beacons.size(); j++) {
							if (d_as[i].angleFi < d_as[j].angleFi) {
								bhelp = beacons[j];
								beacons[j] = beacons[i];
								beacons[i] = bhelp;
								d_ahelp = d_as[j];
								d_as[j] = d_as[i];
								d_as[i] = d_ahelp;
							}
						}
					}

					//double xh1=(double)given_beacons.beacons[beacons[0].which].position.x;
					//double yh1=(double)given_beacons.beacons[beacons[0].which].position.y;
					//double xh2=(double)given_beacons.beacons[beacons[1].which].position.x;
					//double yh2=(double)given_beacons.beacons[beacons[1].which].position.y;

					double xh1 = world_beacons[beacons[0].which].x;
					double yh1 = world_beacons[beacons[0].which].y;
					double xh2 = world_beacons[beacons[1].which].x;
					double yh2 = world_beacons[beacons[1].which].y;
					if (circle_circle_intersection(xh1, yh1, d_as[0].distance, xh2,
							yh2, d_as[1].distance, &x, &y) == 1) {
						rp.x = x;
						rp.y = y;
						// izaberi najbolju kombinaciju
						// koja ce ti dati x and y mozda koristi i for petlju za presjek vise  kruznica
						// kada imas 2 beacons koja vidis i kada znas presjek, racunas ugao
						double x1, x2, y1, y2, alpha1;
						x1 = b1_x - rp.x;
						x2 = b2_x - rp.x;
						y1 = b1_y - rp.y;
						y2 = b2_y - rp.y;

						printf("%f\n", sqrt(x1 * x1 + y1 * y1));
						alpha = acos(x1 / sqrt(x1 * x1 + y1 * y1));
						printf("%f\n", sqrt(x2 * x2 + y2 * y2));
						alpha1 = acos(x2 / sqrt(x2 * x2 + y2 * y2));
						rp.alpha = (d_as[0].angleFi + alpha + d_as[1].angleFi
								+ alpha1) / 2;
						printf("Beacons %f %f\n", rp.x, rp.y);
						return rp;
					} else {
						rp.x = 0.0;
						rp.y = 0.0;
						rp.alpha = 0.0;
						rp.acc = 0;
						rp.distance = 0.0;
						printf("Beacons %f %f\n", rp.x, rp.y);
						return rp;
					}
				} else { //if you can see 3, chose 2 closer and check how third fit in it
					beacon_pos bhelp;
					dist_angle d_ahelp;
					for (int i = 0; i < beacons.size() - 1; i++) {
						for (int j = i + 1; j < beacons.size(); j++) {
							if (d_as[i].angleFi < d_as[j].angleFi) {
								bhelp = beacons[j];
								beacons[j] = beacons[i];
								beacons[i] = bhelp;
								d_ahelp = d_as[j];
								d_as[j] = d_as[i];
								d_as[i] = d_ahelp;
							}
						}
					}
					double xh1 = world_beacons[beacons[0].which].x;
					double yh1 = world_beacons[beacons[0].which].y;
					double xh2 = world_beacons[beacons[1].which].x;
					double yh2 = world_beacons[beacons[1].which].y;
					if (circle_circle_intersection(xh1, yh1, d_as[0].distance, xh2,
							yh2, d_as[1].distance, &x, &y) == 1) {
						rp.x = x;
						rp.y = y;
						double xh3 = world_beacons[beacons[2].which].x;
						double yh3 = world_beacons[beacons[2].which].y;
						//after testing change this
						if (circle_circle_intersection(xh1, yh1, d_as[0].distance,
								xh3, yh3, d_as[2].distance, &x, &y) == 1) {
							rp.x = (rp.x + x) / 2;
							rp.y = (rp.y + y) / 2;
						}
						double x1, x2, y1, y2, alpha1;
						x1 = b1_x - rp.x;
						x2 = b2_x - rp.x;
						y1 = b1_y - rp.y;
						y2 = b2_y - rp.y;

						printf("%f\n", sqrt(x1 * x1 + y1 * y1));
						alpha = acos(x1 / sqrt(x1 * x1 + y1 * y1));
						printf("%f\n", sqrt(x2 * x2 + y2 * y2));
						alpha1 = acos(x2 / sqrt(x2 * x2 + y2 * y2));
						rp.alpha = (d_as[0].angleFi + alpha + d_as[1].angleFi
								+ alpha1) / 2;
						rp.distance = d_as[0].distance;
					} else {
						rp.x = 0.0;
						rp.y = 0.0;
						rp.alpha = 0.0;
						rp.acc = 0;
						rp.distance = 0.0;
						printf("Beacons %f %f\n", rp.x, rp.y);
						return rp;
					}
				}
			}
		}

}



#include <pluginlib/class_list_macros.h>
//PLUGINLIB_EXPORT_CLASS(example_pkg::MyNodeletClass, nodelet::Nodelet)
PLUGINLIB_DECLARE_CLASS(example_pkg, MyNodeletClass, example_pkg::MyNodeletClass, nodelet::Nodelet)
