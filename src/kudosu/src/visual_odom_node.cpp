#include <ros/ros.h>
#include <image_transport/image_transport.h>
#include <cv_bridge/cv_bridge.h>
#include <sensor_msgs/image_encodings.h>
#include <opencv2/imgproc/imgproc.hpp>
#include <nav_msgs/Odometry.h>
#include <sstream>
#include <assign1_2013/beacons.h>
#include <tf/transform_broadcaster.h>
#include <kudosu/Beacons.h>
#include "visual_odom.h"

namespace enc = sensor_msgs::image_encodings;
using namespace cv;
using namespace std;


// Beacon top/bottom colour combinations
const unsigned num_beacons = 6;
unsigned beacon_colours[num_beacons][2] = {
		{ 3, 0 }, // yellow/pink
		{ 0, 3 }, // pink/yellow
		{ 1, 0 }, // blue/pink
		{ 0, 1 }, // pink/blue
		{ 2, 0 }, // green/pink
		{ 0, 2 }, // pink/green
		};

// Minimum and maximum values for each colour (HSV encoding)
const unsigned num_colours = 4;
const string colour_names[num_colours] = { "pink", "blue", "green", "yellow" };
const Scalar colours[num_colours][2] = {
		{ Scalar(145, 45, 45), Scalar(175, 255,255) }, // pink
		{ Scalar(90, 45, 45), Scalar(110, 255, 255) }, // blue
		{ Scalar(70, 20, 20), Scalar(95, 255, 255) }, // green
		{ Scalar(20, 50, 50), Scalar(40, 255, 255) }, // yellow
		};

/*static const Scalar colours[num_colours][2] = {
		{ Scalar(150, 50, 50), Scalar(170, 255,	255) }, // pink
			{ Scalar(90, 50, 50), Scalar(110, 255, 255) }, // blue
			{ Scalar(70, 20, 20), Scalar(90, 255, 255) }, // green
			{ Scalar(20, 50, 50), Scalar(40, 255, 255) }, // yellow
			};*/
// for debugging
const Scalar rgb_colours[4] = { Scalar(255, 0, 255), Scalar(255, 0, 0), Scalar(
		0, 255, 0), Scalar(0, 255, 255) };



double max_x = 3.0; //Max_X
double max_y = 4.5; //Max_Y
double H = 0.2;

b_pos world_beacons[6]; //The real world position of each beacon which matches our beacon_pos.which

ros::Publisher pub_vo;
ros::Publisher beacon_pub;

class ImageConverter {
	ros::NodeHandle nh_;
	image_transport::ImageTransport it_;
	image_transport::Subscriber image_sub_;
	image_transport::Publisher image_pub_;
	image_transport::Publisher image_debug_;
	cv::Mat src, chan_hsv;;
	comp3431::Beacons given_beacons; //TODO: Need to order these in the expected 0-5 ordering used for colour recognition
	int frame;

public:
	ImageConverter() :
			it_(nh_), frame(0) {
		ROS_INFO("Constructor");
		image_pub_ = it_.advertise("out", 1);
		image_debug_ = it_.advertise("/out_debug", 1);
		image_sub_ = it_.subscribe("in", 1, &ImageConverter::imageCb, this);
		//pub_vo = nh_.advertise<nav_msgs::Odometry>("/vo", 0); //check this is correct
		beacon_pub = nh_.advertise <kudosu::Beacons> ("/nav_beacons", 1000);
		loadBeaconsFromFile();
	}

	~ImageConverter() {
	}


	/*Callback from the camera*/
	void imageCb(const sensor_msgs::ImageConstPtr& msg) {
		const double drop_frames = 0.0; // 90%
		++frame;
		if (int(frame * (1 - drop_frames))
				== int((frame - 1) * (1 - drop_frames))) {
			ROS_INFO("Frame (dropped)");
			return;
		} else {
			ROS_INFO("Frame (processed)");
		}
		cv_bridge::CvImagePtr cv_ptr;
		try {
			cv_ptr = cv_bridge::toCvCopy(msg, enc::BGR8);
		} catch (cv_bridge::Exception& e) {
			ROS_ERROR("cv_bridge exception: %s", e.what());
			return;
		}
		cv_ptr->image.copyTo(src);
		vector < beacon_pos > beacons = findBeacons(src, cv_ptr);

		//adjustHeightIncludeWhite(beacons);

		addDebugToImage(beacons);

		/*Find robot position*/
		kudosu::Beacons nav_info;
		unsigned i;
		for (i = 0; i < beacons.size(); ++i) {
			dist_angle da = beacon_relative_pos(beacons[i]);
			nav_info.beacons[i].x = da.distance;
			nav_info.beacons[i].y = da.angleFi;
			nav_info.beacons[i].z = beacons[i].which;
		}
		for(; i < 6; ++i) {
			nav_info.beacons[i].z = -1;
		}
		beacon_pub.publish(nav_info); //Publish our beacon information into the Navigation node

		cv_ptr->image = src;
		image_pub_.publish(cv_ptr->toImageMsg());
	}

	/*
	 * This ad-hoc solution replaces findContours, as using the latter causes memory corruption.
	 * Don't feel bad about this, because using findContours was an ad-hoc solution anyway.
	 */
	static Rect floodFill(Mat input, vector <bool> &visited, int row, int col) {
		vector <pair <int, int> > stack;
		stack.push_back(make_pair(row, col));

		Rect rect(col, row, col, row);
		while(!stack.empty()) {
			int r = stack.back().first, c = stack.back().second;
			stack.pop_back();
			if (visited[r * input.cols + c]) continue;
			visited[r * input.cols + c] = true;
			if (!input.at <unsigned char> (r, c)) continue;

			if (r < rect.y) rect.y = r;
			if (r > rect.height) rect.height = r;
			if (c < rect.x) rect.x = c;
			if (c > rect.width) rect.width = c;

			for (int nr = r-1; nr <= r+1; ++nr) {
				for (int nc = c-1; nc <= c+1; ++nc) {
					if (nr < 0 || nr >= input.rows || nc < 0 || nc >= input.cols) continue;
					stack.push_back(make_pair(nr, nc));
				}
			}
		}
		rect.width = rect.width - rect.x + 1;
		rect.height = rect.height - rect.y + 1;
		return rect;
	}
	static vector <Rect> floodFillRects(Mat input) {
		vector <Rect> rects;
		vector <bool> visited(input.rows * input.cols);
		for (int r = 0; r < input.rows; ++r) {
			for (int c = 0; c < input.cols; ++c) {
				if (visited[r * input.cols + c]) continue;
				if (!input.at <unsigned char> (r, c)) continue;
				rects.push_back(floodFill(input, visited, r, c));
			}
		}
		return rects;
	}

	/*beaconScore - Take two rectangles and assigns a score based on whether they are 'linked'
	 * i.e. whether they should be regarded as part of the same beacon*/
	double beaconScore(const Rect top, const Rect bot, const int screen_width,
			const int screen_height) {
		// calculate "score"
		double score = 0;

		// aspect ratio of each rect (smaller is better)
		double aspect;
		aspect = double(top.height) / top.width;
		if (aspect < 1)
			aspect = 1 / aspect;
		if (aspect > 2)
			return 999;
		score += aspect - 1;
		aspect = double(bot.height) / bot.width;
		if (aspect < 1)
			aspect = 1 / aspect;
		if (aspect > 2)
			return 999;
		score += aspect - 1;

		// difference between top/bottom sizes (nearer is better)
		double top_sz = sqrt(top.width * top.height);
		double bot_sz = sqrt(bot.width * bot.height);
		aspect = top_sz / bot_sz;
		if (aspect < 1)
			aspect = 1 / aspect;
		if (aspect > 2)
			return 999;
		score += aspect - 1;

		// top/bottom separation (nearer is better)
		//double bla = top.x + top.width / 2.0;
		double top_mx = top.x + top.width / 2.0;
		double top_my = top.y + top.height;
		double bot_mx = bot.x + bot.width / 2.0;
		double bot_my = bot.y;
		double average_sz = sqrt(top_sz * bot_sz);
		double screen_sz = sqrt(screen_width * screen_height);
		double tmp = (top_mx - bot_mx) * (top_mx - bot_mx)
				+ (top_my - bot_my) * (top_my - bot_my);
		if (tmp < 0)
			tmp = 0;
		double dist = sqrt(tmp) / average_sz;
		if (dist > 0.5)
			return 999;
		score += dist;

		// size on screen (bigger is better)
		//score -= average_sz / screen_sz /2;

		return score;
	}

	/*findBeacons - Takes the image to the processed and our cv_bridge pointer.  Parses the input image to find
	 * regions which are beacons.  Returns a vector of the best beacons we have identified in the image*/
	vector<beacon_pos> findBeacons(Mat input, cv_bridge::CvImagePtr cv_ptr) {

		//find contours
		std::vector<cv::Vec4i> hierarchy;

		cv::cvtColor(input, chan_hsv, CV_BGR2HSV);

		// Recognise regions of each colour
		std::vector<std::vector<cv::Point> > contours[num_colours];
		std::vector<Rect> boundRect[num_colours];

		Mat input_;
		Mat mask;
		input.copyTo(input_);
		for (unsigned i = 0; i < num_colours; ++i) {
			// Mask out this colour
			cv::inRange(chan_hsv, colours[i][0], colours[i][1], mask);
			//Clean up the image - Remove noise
			cv::erode(mask, mask, cv::Mat(), cv::Point(-1, -1), 2);
			//Fill in the gaps
			cv::dilate(mask, mask, cv::Mat());
			//cv::GaussianBlur(mask, mask, cv::Size(3,3), 0, 0);
			// Find all blobs of this colour
			#if 0
						cv::findContours(mask, contours[i], cv::RETR_EXTERNAL, cv::CHAIN_APPROX_NONE);
						for (unsigned j = 0; j < contours[i].size(); ++j) {
			#if 0
							assert(!contours[i][j].empty());
							int minx, miny, maxx, maxy;
							minx = maxx = contours[i][j][0].x;
							miny = maxy = contours[i][j][0].y;
							Rect br;
							for (unsigned k = 1; k < contours[i][j].size(); ++k) {
								if(contours[i][j][k].x < minx) minx = contours[i][j][k].x;
								if(contours[i][j][k].x > maxx) maxx = contours[i][j][k].x;
								if(contours[i][j][k].y < miny) miny = contours[i][j][k].y;
								if(contours[i][j][k].y > maxy) maxy = contours[i][j][k].y;
							}
							br.x = minx, br.y = miny, br.width = maxx - minx, br.height = maxy - miny;
			#else
							Rect br = boundingRect(Mat(contours[i][j]));
			#endif
							boundRect[i].push_back(br);
							//rectangle(input_, br, rgb_colours[i], 2, 8, 0);
						}
			#else
						vector <Rect> rects = floodFillRects(mask);
						//Try to lock the height detected once we detect a rectangle with a narrow matrix
						for(int j = 0; j < rects.size(); j++){
							Rect br = rects[j];
							rectangle(input_, br, rgb_colours[i], 2, 8, 0);
						}
						boundRect[i] = rects;
			#endif

		}
		cv_ptr->image = input_;
		image_debug_.publish(cv_ptr->toImageMsg());

		// Find regions that look like beacons
		vector<beacon_pos> beacons;
		for (unsigned i = 0; i < num_beacons; ++i) {
			// Just brute-force it for now
			const unsigned top_colour = beacon_colours[i][0], bot_colour =
					beacon_colours[i][1];

			// Find the "best" pair of top and bottom colour regions
			unsigned best_top = 0, best_bot = 0;
			beacon_pos best_beacon;
			best_beacon.which = i;
			double best_score = numeric_limits<double>::infinity();
			for (unsigned j = 0; j < boundRect[top_colour].size(); ++j) {
				for (unsigned k = 0; k < boundRect[bot_colour].size(); ++k) {
					Rect top = boundRect[top_colour][j], bot =
							boundRect[bot_colour][k];
					double score = beaconScore(top, bot, input.cols,
							input.rows);
					if (!(score >= best_score)) {
						best_top = j;
						best_bot = k;
						best_score = score;

						// use distance between rects as the height estimator
						double top_ycentre = top.y + top.height / 2.0;
						double bot_ycentre = bot.y + bot.height / 2.0;
						double top_xcentre = top.x + top.width / 2.0;
						double bot_xcentre = bot.x + bot.width / 2.0;

						double average_xcentre = (top_xcentre + bot_xcentre) / 2 / input.cols;
						best_beacon.x = average_xcentre - 0.5;
						double average_ycentre = (top_ycentre + bot_ycentre) / 2 / input.rows;
						best_beacon.y = average_ycentre - 0.5;

						best_beacon.w = (top.width + bot.width) / 2.0 / input.cols;
						best_beacon.h = -(top_ycentre - bot_ycentre) * 2 / input.rows;
					}
				}
			}
			const double score_threshold = 1;
			if (best_score < score_threshold) {
				beacons.push_back(best_beacon);
			}
		}
		return beacons;
	}

	/*Helper function to publish our visual odometry to /vo*/
	void publish_visual_odometry(const sensor_msgs::ImageConstPtr& msg,
			double x_pos, double y_pos, double heading) {
		static nav_msgs::Odometry odom_output;

		odom_output.pose.pose.position.x = x_pos;
		odom_output.pose.pose.position.y = y_pos;
		odom_output.pose.pose.orientation = tf::createQuaternionMsgFromYaw(heading);
		if(x_pos == 0)
			odom_output.pose.covariance[0] = 999999; //X-X
		else
			odom_output.pose.covariance[0] = 0.000001; //X-X
		if(y_pos == 0)
			odom_output.pose.covariance[7] = 999999; //Y-Y
		else
			odom_output.pose.covariance[7] = 0.000001; //Y-Y
		odom_output.pose.covariance[14] = 999999; //Z-Z
		odom_output.pose.covariance[21] = 999999; //Roll-Roll
		odom_output.pose.covariance[28] = 999999; //Pitch-Pitch
		if(heading == 0)
			odom_output.pose.covariance[35] = 999999; //Yaw-Yaw
		else
			odom_output.pose.covariance[35] = 0.000001; //Yaw-Yaw
		odom_output.twist.covariance = odom_output.pose.covariance;

		odom_output.header.stamp = msg->header.stamp;
		odom_output.header.seq++;
		//Note: We assume we will never be at 0,0,0 exactly except at the start of the run
		//So if we do not have a reading for X, Y or Z, set its covariance high so it is ignored
		if(x_pos || y_pos || heading) //don't publish if we have nothing to add.
			pub_vo.publish(odom_output);
	}

	/*loadBeaconsFromFile - Loads the beacon positions into our array based on Matt's input file
	 * This is for convenience as we identify each beacon by an int*/
	void loadBeaconsFromFile() {
		int num_beacons = given_beacons.beacons.size();
		if (!num_beacons) {
			ROS_INFO("No beacons are loaded");
			return;
		}
		for (int i = 0; i < num_beacons; i++) {
			comp3431::Beacon beac = given_beacons.beacons[i];
			if (beac.top == "yellow" && beac.bottom == "pink") {
				world_beacons[0].x = beac.position.x;
				world_beacons[0].y = beac.position.y;
			} else if (beac.top == "pink" && beac.bottom == "yellow") {
				world_beacons[1].x = beac.position.x;
				world_beacons[1].y = beac.position.y;
			} else if (beac.top == "blue" && beac.bottom == "pink") {
				world_beacons[2].x = beac.position.x;
				world_beacons[2].y = beac.position.y;
			} else if (beac.top == "pink" && beac.bottom == "blue") {
				world_beacons[3].x = beac.position.x;
				world_beacons[3].y = beac.position.y;
			} else if (beac.top == "green" && beac.bottom == "pink") {
				world_beacons[4].x = beac.position.x;
				world_beacons[4].y = beac.position.y;
			} else if (beac.top == "pink" && beac.bottom == "green") {
				world_beacons[5].x = beac.position.x;
				world_beacons[5].y = beac.position.y;
			}
			ROS_INFO("Loaded %f %f", beac.position.x, beac.position.y);
		}
	}

	/*Note we assume that the beacon at (x0, y0) is the leftmost in the image
	 * so we can return only a single intersect point*/
	static int circle_circle_intersection(double x0, double y0, double r0,
			double x1, double y1, double r1, double *x, double *y) {
		double a, dx, dy, d, h, rx, ry;
		double x2, y2;
		double xi, yi, xi_prime, yi_prime;

		dx = x1 - x0;
		dy = y1 - y0;

		/* Determine the straight-line distance between the centers. */
		d = hypot(dx, dy);

		/* Check for solvability. */
		if (d > (r0 + r1)) {
			/* no solution. circles do not intersect. */
			return 0;
		}
		if (d < fabs(r0 - r1)) {
			/* no solution. one circle is contained in the other */
			return 0;
		}

		/* Determine the distance from point 0 to point 2. */
		a = ((r0 * r0) - (r1 * r1) + (d * d)) / (2.0 * d);

		/* Determine the coordinates of point 2. */
		x2 = x0 + (dx * a / d);
		y2 = y0 + (dy * a / d);

		/* Determine the distance from point 2 to either of the
		 * intersection points.
		 */
		h = sqrt((r0 * r0) - (a * a));

		/* Now determine the offsets of the intersection points from
		 * point 2.
		 */
		rx = -dy * (h / d);
		ry = dx * (h / d);

		/* Determine the absolute intersection points. */
		xi = x2 + rx;
		xi_prime = x2 - rx;
		yi = y2 + ry;
		yi_prime = y2 - ry;

		/*Validation and pick the correct intersection point from the Robots POV*/
		if (abs(xi) > max_x || abs(yi) > max_y) { //circle one outside field
			if (abs(xi_prime) > max_x || abs(yi_prime) > max_y) {
				return (-1); //	both points are out of the field
			} else {
				*x = xi_prime;
				*y = yi_prime;
				return 1;
			}
		}
		else if (abs(xi_prime) > max_x || abs(yi_prime) > max_y) {
			*x = xi; //circle 2 outside field
			*y = yi;
			return -1;
		}
		else { // need to decide from what is left and right from the robot's frame
			if (xi_prime - xi != 0) {
				if ((yi + (yi_prime - yi) * (x0 - xi) / (xi_prime - xi)) > 0) {
					*x = xi;
					*y = yi;
					return 1;
				} else {
					*x = xi_prime;
					*y = yi_prime;
					return 1;
				}
			if (yi_prime - yi > 0) {
				if (xi < x0) {
					*x = xi;
					*y = yi;
					return 1;
				} else {
					*x = xi_prime;
					*y = yi_prime;
					return 1;
				}
			} else {
					if (xi > x0) {
						*x = xi;
						*y = yi;
						return 1;
					} else {
						*x = xi_prime;
						*y = yi_prime;
						return 1;
					}
				}
			}
		}
		return 1;
	}

	// calculate distance and bearing to beacon
	static dist_angle beacon_relative_pos(const beacon_pos pos) {
		dist_angle d_a;
		//printf("Old distance %f, new distance %f \n", d_a.distance, beacon_distance_linear_regression(pos));

		// crude approximation
		const double camera_fov = 57.0 / 180 * M_PI;
		d_a.angleFi = pos.x * camera_fov;

		// magic coefficient
		const double height_distance = 0.24;
		const double camera_depth = 0.30;
		double orig_distance =height_distance / pos.h - camera_depth;
		d_a.distance = height_distance / pos.h - camera_depth;

		// more magic camera angular aberration correction
		const double aberration = 0.25; // 25% change from centreline
		double factor = fabs(d_a.angleFi / (camera_fov/2)) * aberration;
		d_a.distance *= 1 + factor;

		printf("Original D %f factor %f", orig_distance, factor);

		return d_a;
	}


	//Returns estimated distance in metres based on Beacon position in camera frame
	static double beacon_distance_verica(const beacon_pos pos){
		//Calculated with M5P Model tree using Weka - not better than a linear function in practice
		double h = pos.h;
		if(h <= 0.154)
			return 0.6491 * fabs(pos.x) - (14.525/2) * h + 3.4745;
		else if(h <= 0.191){
			return 0.2471 * fabs(pos.x) - 5.4016 * h + 2.2296;
		}
		else if(h <= 0.24){
			return 0.2569 * fabs(pos.x) - 5.5573 * h + 2.2363;
		}
		return 0.2325 * fabs(pos.x) - 5.1772 * h + 2.1072;
	}


	static double beacon_angle_verica(const beacon_pos pos){
		//Calculated with M5P Model tree using Weka - not better than a linear function in practice
				return 1.1007 * pos.x + 0.2432 * pos.h - 0.0461;
	}

	//order_beacons - Order beacons from left to right in the image
	static void order_beacons(vector<beacon_pos> &beacons,
			                  vector<dist_angle> &d_as) {
		if (beacons.size() > 1) {
			beacon_pos bhelp;
			dist_angle d_ahelp;
			for (int i = 0; i < beacons.size() - 1; i++) {
				for (int j = i + 1; j < beacons.size(); j++) {
					if (d_as[i].angleFi < d_as[j].angleFi) {
						bhelp = beacons[j];
						beacons[j] = beacons[i];
						beacons[i] = bhelp;
						d_ahelp = d_as[j];
						d_as[j] = d_as[i];
						d_as[i] = d_ahelp;
					}
				}
			}
		}
	}




	/*addDebugToImage We add some debug shapes and text to the output images on /out topic */
	void addDebugToImage(vector<beacon_pos>& beacons) {
		const Scalar rgb_colours[4] = { Scalar(255, 0, 255), Scalar(255, 0, 0),
				Scalar(0, 255, 0), Scalar(0, 255, 255) };
		for (unsigned i = 0; i < beacons.size(); ++i) {
			{
				cv::Point c((beacons[i].x + 0.5) * src.cols,
						(beacons[i].y + 0.5 - beacons[i].h / 4) * src.rows);
				cv::circle(src, c, beacons[i].w / 2.0 * src.cols,
						rgb_colours[beacon_colours[beacons[i].which][0]], 3, 8,
						0);
				stringstream out, out2;
				/*out << "XY" << (beacons[i].x + 0.5) * src.cols << " "
						<< (beacons[i].y + 0.5) * src.rows << "HxW"
						<< beacons[i].h / 2.0 * src.cols << " "
						<< beacons[i].w / 2.0 * src.cols;*/
				out << "XY" << beacons[i].x << " "
										<< beacons[i].y  << "HxW"
										<< beacons[i].h << " "
										<< beacons[i].w ;
				//range = -0.5 .. 0.5
				cv::putText(src, out.str(), Point(200,320),
						CV_FONT_HERSHEY_COMPLEX, 0.5, Scalar(255, 255, 0));
			}
			{
				cv::Point c((beacons[i].x + 0.5) * src.cols,
						(beacons[i].y + 0.5 + beacons[i].h / 4) * src.rows);
				cv::circle(src, c, beacons[i].w / 2.0 * src.cols,
						rgb_colours[beacon_colours[beacons[i].which][1]], 3, 8,
						0);
			}
		}
	}


	/*Adjust the height of the beacons to capture the total height from top to white base*/
	void adjustHeightIncludeWhite(vector<beacon_pos> beacons) {
		printf("adjust height\n");
		//Mat gray;
		//cvtColor(src, gray, CV_BGR2GRAY);
		beacon_pos curr_beac;
		for (int j = 0; j < beacons.size(); j++) {
			curr_beac = beacons[j];
			int normalX = curr_beac.x * chan_hsv.cols + chan_hsv.cols / 2;
			int normalY = curr_beac.y * chan_hsv.rows + chan_hsv.rows / 2;
			int midpoint = floor(normalX);
			printf("Midpoint %d\n", midpoint);
			int maxWhite = -1;
			//Scalar values = mean(
			cv::Rect roi = cv::Rect(normalX, normalY, curr_beac.h*chan_hsv.rows+5, curr_beac.w*chan_hsv.cols);
			for (int i = normalY; i < chan_hsv.rows; i++) { //Look down the centre row of the beacon
				Vec3b bgrPixel = chan_hsv.at<Vec3b>(i, midpoint);
				int H = bgrPixel.val[0];
				int S = bgrPixel.val[1];
				int V = bgrPixel.val[2];
				bgrPixel.val[0] = 0;
				bgrPixel.val[1] = 0;
				bgrPixel.val[1] = 0;
				if (maxWhite > 0 && H > 25 && H < 30 && V > 70){
					printf("Rejected");
					break; //We stop looking for maxWhite once we hit the wood block under each beacon
				}
				if (S < 45 && V > 100) { //loook for white V and S most important
					maxWhite = i;
					src.at<Vec3b>(i, midpoint) = bgrPixel;
				}
				printf("Pixel Val is (%d %d %d) at (%d, %d), maxW %d\n", H, S, V, midpoint, i, maxWhite);

			}
		}
		//src = gray;

	}
};

int main(int argc, char** argv) {
	ros::init(argc, argv, "visual_odometry");
	ImageConverter ic;
	ROS_INFO("Node Start");
	ros::spin();
	return 0;
}
