/*struct example_pos {
	double x, y; // from centre of screen, range = -0.5 .. 0.5
	double h, factor, distance; // height, factor for scaling and real distance
};*/

struct example_pos {
		double x, y; // from centre of screen, range = -0.5 .. 0.5
		double h, w, factor; // height, factor for scaling
		double distanceX, distanceY, angleFi;// real distance X, Y and angle between head and beacon
	};


struct beacons {
	//unsigned which; // which beacon (0 .. 5)
	double x, y;// from centre
};

//Convenience struct for robot position
struct robot_pos {
	double x, y; // absolute x, y coordinate in the world
	double alpha; //bearing from x-axis+
	double distance; //distance to closest beacon
	double acc; //accuracy
};

// beacon position on camera screen
struct beacon_pos {
	double x, y; // from centre of screen, range = -0.5 .. 0.5
	double w, h; // fraction of screen size, range 0 .. 1
	unsigned which; // which beacon (0 .. 5)
};

struct best_fit{
	double factor, angleFi;
};
struct dist_angle{
	double distance, angleFi;
};

struct b_pos{
	double x, y;
};

