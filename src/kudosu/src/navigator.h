struct example_pos {
		double x, y; // from centre of screen, range = -0.5 .. 0.5
		double h, w, factor; // height, factor for scaling
		double distanceX, distanceY, angleFi;// real distance X, Y and angle between head and beacon
	};


struct beacons {
	//unsigned which; // which beacon (0 .. 5)
	double x, y;// from centre
};

//Convenience struct for robot position
struct robot_pos {
	double x, y; // absolute x, y coordinate in the world
	double alpha; //bearing from x-axis+
	double distance; //distance to closest beacon
	double acc; //accuracy
};

struct best_fit{
	double factor, angleFi;
};
// beacon pos relative to robot
struct beacon_pos {
	double distance, angleFi;
	int which;
};

struct b_pos{
	double x, y;
};
